<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="java.util.ArrayList,jp.mnt.dto.ItemDTO" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>買い物カゴ</title>
<!-- jQuery 3.1 -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script>
<link href="http://cdn.phpoll.com/css/animate.css" rel="stylesheet">

<!-- bootstrap4 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
</head>
<body>
	<jsp:include page="jsp/menu.jsp" />
	<div class="container">
	<h2>カートの中にある商品</h2>
	</div>
	<br>

	<div class="container">
	<table class="table" >


	<% int index = 0;%>
	<% for(ItemDTO dto : (ArrayList<ItemDTO>)session.getAttribute("cart_item")){ %>
	<%	ArrayList<String[]> strList = (ArrayList<String[]>)session.getAttribute("word_list");
		String[] words = strList.get(index);%>
		<tr>
			<div class="panel panel-default">
			<div class="panel-body">
			<div class="row">
			<form name="detailForm" action="detail" method="post">
				<div class="col-md-2"><input type="hidden" name="detail" value="<%= dto.getItemId()%>">
				<img onclick="submitHandler(<%= dto.getItemId()%>);" style="width: 130px; height: 80px" src="getimage?id=<%= dto.getItemId()%>" onerror="src='image/no_image.png'"></div>
			</form>
			<div class="col-md-9">
			<div class="row">
			<div class="col-md-4"><%= words[0] %></div>
			<div class="col-md-4"><%= words[2] %></div>
			<div class="col-md-12">

			<a href="#" onclick="submitHandler(<%= dto.getItemId()%>);">
			<%= dto.getItemName() %>
			</a>
			</div>
			<div class="col-md-4"><%= dto.getArtist()%></div>
			<div class="col-md-4"><%= dto.getPrice() %>円</div>

			<form action = "cartdelete" method = "POST">
				<div class="col-md-4">
					<input type = "hidden" name = item value = "<%= dto.getItemId()%>">
					<input type="submit" value="削除"></div>
			</form>

			</div>
			</div>
			<div class="col-md-1">

			</div>
			</div>
			</div>
			</div>
		</tr>
		<% index++; %>
	<%} %>

	<center>
		<% ArrayList<ItemDTO> list = (ArrayList<ItemDTO>)session.getAttribute("cart_item");
			if(list.size() > 0){
		%>
			<form action = "agenda" method = "POST">
			<input type = "submit" value = "レンタルする" style = "display: block; width: 150px;height: 50px">
			</form>
		<% } else{%>
			<h1>カートに商品が入っていません。</h1>
			<form action = "home" method = "POST">
			<input type = "submit" value = "ホームへ戻る" style = "display: block; width: 150px;height: 50px">
			</form>
		<% } %>
	</center>

<jsp:include page="jsp/footer.jsp" />
</body>
</html>