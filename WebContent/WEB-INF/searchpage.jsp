<%@page import="jp.mnt.dao.SearchDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="jp.mnt.dto.CategoryDTO,java.util.ArrayList,jp.mnt.dto.GenreDTO,jp.mnt.dto.ItemDTO"%>
<%@ page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>商品検索</title>
<!-- jQuery 3.1 -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script>
<script type="text/javascript" src="js/categoryMatchGenre.js"></script>
<link href="http://cdn.phpoll.com/css/animate.css" rel="stylesheet">

<!-- bootstrap3 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<script type="text/javascript">
	function submitHandler(detail) {
	    var form = document.forms["detailForm"];

	    form.detail.value = detail;
	    form.submit();
	}
	</script>
<body>
	<jsp:include page="jsp/menu.jsp" />
	<br>

	<div class="container">
		<h4>商品検索</h4>


		<form name="search" action="search" method="post">

			<select class="category" name="category">
				<option value="0" selected>選択してください</option>
				<c:forEach var="cat" items="${sessionScope.category_list}">
					<option value="<c:out value="${cat.categoryId}" />"><c:out value="${cat.categoryName}" /></option>
				</c:forEach>
			</select> <select class="genre" name="genre" disabled>
				<option value="0" selected>選択してください</option>
				<c:forEach var="gen" items="${sessionScope.genre_list}">
					<option value="<c:out value="${gen.genreId}" />" data-val="${gen.categoryId}"><c:out
							value="${gen.genreName}" /></option>
				</c:forEach>
			</select>

			<!--
		<select name="category" id="sb1" onchange="box2.make(this.value);"></select>
		<select name="genre" id="sb2" onchange="box3.make(this.value);"></select>
		-->
			<input type="text" name="item_"> <input type="submit" value="検索">
		</form>
	</div>
	<br>

	<div class="container">
		<h2>検索結果</h2>
	</div>
	<br>

	<%
		if (request.getAttribute("message") != null) {
	%>
	<%
		if ("".equals(request.getAttribute("message"))) {
	%>
	<div class="container">
		<table class="table">

			<%
				String eq = "";
			%>


			<%
				for (ItemDTO dto : (ArrayList<ItemDTO>) session.getAttribute("item_list")) {
			%>

			<tr>
				<div class="card panel-default">
					<div class="card-body">
						<div class="row">
							<div class="col-md-2">
								<form name="detailForm" action="detail" method="post">
									<input type="hidden" name="detail" value="<%=dto.getItemId()%>"> <img
										onclick="submitHandler(<%=dto.getItemId()%>);" style="width: 130px; height: 80px"
										src="getimage?id=<%=dto.getItemId()%>" onerror="src='image/no_image.png'">
								</form>
							</div>
							<div class="col-md-9">
								<div class="row">
									<div class="col-md-4"><%=dto.getArtist()%></div>
									<div class="col-md-12">
										<a href="#" onclick="submitHandler(<%=dto.getItemId()%>);"> <%=dto.getItemName()%>
										</a>
									</div>
									<div class="col-md-4"><%=dto.getPrice()%>円
									</div>
									<div class="col-md-4">
										<button type="submit" class="btn btn-success" onclick="submitHandler(<%=dto.getItemId()%>);">詳細へ</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</tr>
			<%
				}
			%>
		</table>
	</div>
	<%
		} else {
	%>
	<div><%=(String) request.getAttribute("message")%></div>

	<%
		}
	%>
	<%
		}
	%>

	<jsp:include page="jsp/footer.jsp" />
</body>
</html>