<%--株式会社ソフトワン 峰勇気 --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="jp.mnt.dto.CategoryDTO,java.util.ArrayList,jp.mnt.dto.GenreDTO,jp.mnt.dto.ItemDTO"%>
<%@ page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>ホーム画面</title>
<!-- jQuery 3.1 -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script>
<link href="http://cdn.phpoll.com/css/animate.css" rel="stylesheet">
<!-- bootstrap4 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script type="text/javascript">
	function submitHandler(detail) {
		var form = document.forms["detailForm"];

		form.detail.value = detail;
		form.submit();
	}
</script>
<script type="text/javascript" src="js/categoryMatchGenre.js"></script>
<style type="text/css">
.container {
	margin-top: 30px;
}

.row {
	margin-bottom: 30px;
}

.card {
	margin-left: 5px;
	margin-right: 5px;
	margin-bottom: 10px;
}

.card-title {
	font-size: 1em;
	margin-bottom: 1px;
	margin-left: 4px;
}

.card-header {
	padding: 6px;
}

.col-xs-1-5, .col-sm-1-5, .col-md-1-5, .col-lg-1-5, .col-xs-2-5,
	.col-sm-2-5, .col-md-2-5, .col-lg-2-5, .col-xs-3-5, .col-sm-3-5,
	.col-md-3-5, .col-lg-3-5, .col-xs-4-5, .col-sm-4-5, .col-md-4-5,
	.col-lg-4-5 {
	position: relative;
	min-height: 1px;
	padding-right: 5px;
	padding-left: 5px;
}

.col-xs-1-5 {
	width: 20%;
	float: left;
}

.col-xs-2-5 {
	width: 40%;
	float: left;
}

.col-xs-3-5 {
	width: 60%;
	float: left;
}

.col-xs-4-5 {
	width: 80%;
	float: left;
}

@media ( min-width : 768px) {
	.col-sm-1-5 {
		width: 17%;
		float: left;
	}
	.col-sm-2-5 {
		width: 40%;
		float: left;
	}
	.col-sm-3-5 {
		width: 60%;
		float: left;
	}
	.col-sm-4-5 {
		width: 80%;
		float: left;
	}
}

@media ( min-width : 992px) {
	.col-md-1-5 {
		width: 18%;
		float: left;
	}
	.col-md-2-5 {
		width: 40%;
		float: left;
	}
	.col-md-3-5 {
		width: 60%;
		float: left;
	}
	.col-md-4-5 {
		width: 80%;
		float: left;
	}
}

@media ( min-width : 1200px) {
	.col-lg-1-5 {
		width: 19%;
		float: left;
	}
	.col-lg-2-5 {
		width: 40%;
		float: left;
	}
	.col-lg-3-5 {
		width: 60%;
		float: left;
	}
	.col-lg-4-5 {
		width: 80%;
		float: left;
	}
}
</style>
</head>

<body>
	<jsp:include page="jsp/menu.jsp" />
	<div class="container">
		<div class="row">

			<form name="search" action="search" method="POST">
				<select class="category" name="category">
					<option value="0" selected>選択してください</option>
					<c:forEach var="cat" items="${sessionScope.category_list}">
						<option value="<c:out value="${cat.categoryId}" />"><c:out
								value="${cat.categoryName}" /></option>
					</c:forEach>
				</select> <select class="genre" name="genre" disabled>
					<option value="0" selected>選択してください</option>
					<c:forEach var="gen" items="${sessionScope.genre_list}">
						<option value="<c:out value="${gen.genreId}" />" data-val="${gen.categoryId}"><c:out
								value="${gen.genreName}" /></option>
					</c:forEach>
				</select> <input type="text" name="item_" /> <input type="submit" value="検索" />
			</form>

		</div>
		<h1>おススメ商品</h1>
		<div class="row">
			<%
				int index = 0;
				ArrayList<String[]> list = (ArrayList<String[]>) session.getAttribute("recommend_word");
			%>
			<%
				for (ItemDTO dto : (ArrayList<ItemDTO>) session.getAttribute("item_list")) {
			%>

			<section class="card col-lg-1-5 col-md-1-5 col-sm-1-5 col-xs-12">
				<div class="card-header" style="height: 70px;">
					<span class="badge badge-info"><%=dto.getNewAndOldName()%></span> <span
						class="badge badge-default"><%=dto.getCategoryName()%></span>
					<h1 class="card-title"><%=dto.getItemName()%></h1>
				</div>
				<img class="card-img" src="getimage?id=<%=dto.getItemId()%>" onerror="src='image/no_image.png'"
					alt="" height="150px" width="auto">
				<div class="card-content">
					<p class="card-text">
						価格：<%=dto.getPrice()%>円
					</p>
				</div>
				<div class="card-link">
					<form name="detail" action="detail" method="POST">
						<input type="hidden" name="detail" value="<%=dto.getItemId()%>">
						<button type="submit" class="btn btn-success btn-block">詳細</button>
					</form>
					<form name="cart" action="cart" method="POST">
						<input type="hidden" name="item" value="<%=dto.getItemId()%>">
						<button type="submit" class="btn btn-warning btn-block">カートへ</button>
					</form>
				</div>
			</section>
			<%
				index++;
			%>
			<%
				if (index >= 10)
						break;
			%>
			<%
				}
			%>
		</div>
	</div>

	<%-- セッションに保存した情報の破棄--%>
	<%
		session.removeAttribute("item_list");
		session.removeAttribute("recommend_word");
	%>

	<footer class="footer">
		<div class="container">
			<p class="text-muted text-center">copyright © 2017 music & movie rental Co., Ltd All rights
				reserved.</p>
		</div>
	</footer>
</body>
</html>