<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>商品詳細画面</title>
<!-- jQuery 3.1 -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script>
<link href="http://cdn.phpoll.com/css/animate.css" rel="stylesheet">
<!-- bootstrap3 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<link href="css/style.css" rel="stylesheet">
</head>

<body>
	<jsp:include page="jsp/menu.jsp" />
	<div class="container">
		<div class="row">


			<table class="table">
				<tr>
					<td rowspan="7"><img style="width: 250px; height: 300px"
						src="getimage?id=${ item.itemId }"
						onerror="src='image/no_image.png'"></td>
				</tr>
				<tr>
					<td><span class="badge badge-default">${ words[0] }</span> ${ words[2]}</td>
				</tr>
				<tr>
					<td>${words[1]}</td>
				</tr>
				<tr>
					<td>${item.itemName}</td>
				</tr>
				<tr>
					<td>${item.artist}</td>
				</tr>
				<tr>
					<td>${item.price}円</td>
				</tr>
				<tr>
					<td>${item.remarks}</td>
				</tr>
			</table>
		</div>
		<div>
			<form action="cart" method="POST">
				<input type="hidden" name="item" value="${item.itemId}" />
				<button type="submit" class="btn btn-warning btn-block">カートへ</button>
			</form>
		</div>
	</div>
	<jsp:include page="jsp/footer.jsp" />
</body>
</html>