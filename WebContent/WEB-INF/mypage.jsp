<%--株式会社ソフトワン　峰勇気 --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>マイページ</title>
<!-- jquery読み込み -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
<!-- タブ切り替えのjavascript -->
<script type="text/javascript">
	//編集モードにする。
	function editMode() {
		//クラス名がb1のテキストボックスを編集可能にする。
		target = "b1"
		obj = document.getElementsByClassName(target);
		for (i = 0; i < obj.length; i++) {
			obj[i].readOnly = false;
		}

		show();
		hidden();
	}

	//表示
	function show() {
		target = "edit";
		btn = document.getElementsByClassName(target);
		for (i = 0; i < btn.length; i++) {
			btn[i].style.display = "block";
		}
	}

	//非表示
	function hidden() {
		target = "hidden";
		btn = document.getElementsByClassName(target);
		for (i = 0; i < btn.length; i++) {
			btn[i].style.display = "none";
		}
	}

	//入力チェック
	function check() {
		//変数宣言
		var result = true;

		// エラー用装飾のためのクラスリセット
		$('#lastName').removeClass("inp_error");
		$('#firstName').removeClass("inp_error");
		$('#mail1').removeClass("inp_error");
		$('#mail2').removeClass("inp_error");
		$('#pass1').removeClass("inp_error");
		$('#pass2').removeClass("inp_error");
		$('#postalCode').removeClass("inp_error");
		$('#address1').removeClass("inp_error");
		$('#address2').removeClass("inp_error");
		$('#tel').removeClass("inp_error");
		$('#birthday').removeClass("inp_error");
		$('#cardNum').removeClass("inp_error");
		$('#cardName').removeClass("inp_error");
		$('#expirationM').removeClass("inp_error");
		$('#expirationY').removeClass("inp_error");

		// 入力エラー文をリセット
		$("#lastName_error").empty();
		$("#firstName_error").empty();
		$("#mail1_error").empty();
		$("#mail2_error").empty();
		$("#pass1_error").empty();
		$("#pass2_error").empty();
		$("#postalCode_error").empty();
		$("#address1_error").empty();
		$("#address2_error").empty();
		$("#tel_error").empty();
		$("#birthday_error").empty();
		$("#cardNum_error").empty();
		$("#cardName_error").empty();
		$("#expirationM_error").empty();
		$("#expirationY_error").empty();

		// 入力内容セット
		var lastName = $("#lastName").val();
		var firstName = $("#firstName").val();
		var mail1 = $("#mail1").val();
		var mail2 = $("#mail2").val();
		var pass1 = $("#pass1").val();
		var pass2 = $("#pass2").val();
		var postalCode = $("#postalCode").val();
		var address1 = $("#address1").val();
		var address2 = $("#address2").val();
		var tel = $("#tel").val();
		var birthday = $("#birthday").val();
		var cardNum = $("#cardNum").val();
		var cardName = $("#cardName").val();
		var expirationM = $("#expirationM").val();
		var expirationY = $("#expirationY").val();

		//入力内容チェック
		//姓
		if (lastName == "") {
			$("#lastName_error").html(
					"<i class='fa fa-exclamation-circle'></i> 入力は必須です。");
			$("#lastName").addClass("inp_error");
			result = false;
		} else if (lastName.length > 20) {
			$("#lastName_error")
					.html(
							"<i class='fa fa-exclamation-circle'></i> 20文字以内で入力してください。");
			$("#lastName").addClass("inp_error");
			result = false;
		}

		//名
		if (firstName == "") {
			$("#firstName_error").html(
					"<i class='fa fa-exclamation-circle'></i> 入力は必須です。");
			$("#firstName").addClass("inp_error");
			result = false;
		} else if (fristName.length > 20) {
			$("#firstName_error")
					.html(
							"<i class='fa fa-exclamation-circle'></i> 20文字以内で入力してください。");
			$("#firstName").addClass("inp_error");
			result = false;
		}

		//メールアドレス
		if (mail1 == "") {
			$("#mail1_error").html(
					"<i class='fa fa-exclamation-circle'></i> 入力は必須です。");
			$("#mail1").addClass("inp_error");
			result = false;
		} else if (!mail1
				.match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/)) {
			$('#mail1_error')
					.html(
							"<i class='fa fa-exclamation-circle'></i> 正しいメールアドレスを入力してください。");
			$("#mail1").addClass("inp_error");
			result = false;
		} else if (mail1.length > 30) {
			$("#mail1_error")
					.html(
							"<i class='fa fa-exclamation-circle'></i> 30文字以内で入力してください。");
			$("#mail1").addClass("inp_error");
			result = false;
		}

		//メールアドレス(確認)
		if (mail2 == "") {
			$("#mail2_error").html(
					"<i class='fa fa-exclamation-circle'></i> 入力は必須です。");
			$("#mail2").addClass("inp_error");
			result = false;
		} else if (!mail2
				.match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/)) {
			$('#mail2_error')
					.html(
							"<i class='fa fa-exclamation-circle'></i> 正しいメールアドレスを入力してください。");
			$("#mail2").addClass("inp_error");
			result = false;
		} else if (mail2.length > 30) {
			$("#mail2_error")
					.html(
							"<i class='fa fa-exclamation-circle'></i> 30文字以内で入力してください。");
			$("#mail2").addClass("inp_error");
			result = false;
		} else if (mail1 == mail2) {
			$("#mail2_error").html(
					"<i class='fa fa-exclamation-circle'></i> メールアドレスが一致しません。");
			$("#mail2").addClass("inp_error");
		}

		//パスワード
		if (mail2 == "") {
			$("#pass1_error").html(
					"<i class='fa fa-exclamation-circle'></i> 入力は必須です。");
			$("#pass1").addClass("inp_error");
			result = false;
		} else if (mail2.length > 40) {
			$("#pass1_error")
					.html(
							"<i class='fa fa-exclamation-circle'></i> 40文字以内で入力してください。");
			$("#pass1").addClass("inp_error");
			result = false;
		}

		//パスワード(確認)
		if (pass2 == "") {
			$("#pass2_error").html(
					"<i class='fa fa-exclamation-circle'></i> 入力は必須です。");
			$("#pass2").addClass("inp_error");
			result = false;
		} else if (mail2.length > 40) {
			$("#pass2_error")
					.html(
							"<i class='fa fa-exclamation-circle'></i> 40文字以内で入力してください。");
			$("#pass2").addClass("inp_error");
			result = false;
		} else if (pass1 == pass2) {
			$("#pass2_error").html(
					"<i class='fa fa-exclamation-circle'></i> パスワードが一致しません。");
			$("#pass2").addClass("inp_error");
		}

		//郵便番号
		if (postalCode == "") {
			$("#postalCode_error").html(
					"<i class='fa fa-exclamation-circle'></i> 入力は必須です。");
			$("#postalCode").addClass("inp_error");
			result = false;
		} else if (postalCode.length != 7) {
			$("#postalCode_error").html(
					"<i class='fa fa-exclamation-circle'></i> 7文字で入力してください。");
			$("#postalCode").addClass("inp_error");
			result = false;
		}

		//住所1
		if (address1 == "") {
			$("#address1_error").html(
					"<i class='fa fa-exclamation-circle'></i> 入力は必須です。");
			$("#address1").addClass("inp_error");
			result = false;
		} else if (address1.length > 50) {
			$("#address1_error")
					.html(
							"<i class='fa fa-exclamation-circle'></i> 50文字以内で入力してください。");
			$("#address1").addClass("inp_error");
			result = false;
		}

		//住所2
		if (address2 == "") {
			$("#address2_error").html(
					"<i class='fa fa-exclamation-circle'></i> 入力は必須です。");
			$("#address2").addClass("inp_error");
			result = false;
		} else if (address1.length > 50) {
			$("#address2_error")
					.html(
							"<i class='fa fa-exclamation-circle'></i> 50文字以内で入力してください。");
			$("#address2").addClass("inp_error");
			result = false;
		}

		//電話番号
		if (tel == "") {
			$("#tel_error").html(
					"<i class='fa fa-exclamation-circle'></i> 入力は必須です。");
			$("#tel").addClass("inp_error");
			result = false;
		} else if (tel.length > 11) {
			$("#tel_error")
					.html(
							"<i class='fa fa-exclamation-circle'></i> 11文字以内で入力してください。");
			$("#tel").addClass("inp_error");
			result = false;
		}

		//生年月日
		if (birthday == "") {
			$("#birthday_error").html(
					"<i class='fa fa-exclamation-circle'></i> 入力は必須です。");
			$("#birthday").addClass("inp_error");
			result = false;
		}

		//カード番号
		if (cardNum == "") {
			$("#cardNum_error").html(
					"<i class='fa fa-exclamation-circle'></i> 入力は必須です。");
			$("#cardNum").addClass("inp_error");
			result = false;
		} else if (cardNum.length > 16) {
			$("#cardNum_error").html(
					"<i class='fa fa-exclamation-circle'></i> 16桁以内で入力してください。");
			$("#cardNum").addClass("inp_error");
			result = false;
		}

		//カード名義
		if (cardName == "") {
			$("#cardName_error").html(
					"<i class='fa fa-exclamation-circle'></i> 入力は必須です。");
			$("#cardName").addClass("inp_error");
			result = false;
		} else if (!cardName.match("^[A-Z]+$")) {
			$('#cardName_error')
					.html(
							"<i class='fa fa-exclamation-circle'></i> 正しいメールアドレスを入力してください。");
			$("#cardName").addClass("inp_error");
			result = false;
		} else if (cardName.length > 50) {
			$("#cardName_error").html(
					"<i class='fa fa-exclamation-circle'></i> 50桁以内で入力してください。");
			$("#cardName").addClass("inp_error");
			result = false;
		}

		//有効期限(月)
		if (expirationM == "") {
			$("#expirationM_error").html(
					"<i class='fa fa-exclamation-circle'></i> 入力は必須です。");
			$("#expirationM").addClass("inp_error");
			result = false;
		} else if (expirationM.length != 2) {
			$("#expirationM_error").html(
					"<i class='fa fa-exclamation-circle'></i> 2桁で入力してください。");
			$("#expirationM").addClass("inp_error");
			result = false;
		}

		//有効期限(年)
		if (expirationY == "") {
			$("#expirationY_error").html(
					"<i class='fa fa-exclamation-circle'></i> 入力は必須です。");
			$("#expirationY").addClass("inp_error");
			result = false;
		} else if (expirationY.length != 4) {
			$("#expirationY_error").html(
					"<i class='fa fa-exclamation-circle'></i> 4桁で入力してください。");
			$("#expirationY").addClass("inp_error");
			result = false;
		}

		return result;
	}
</script>

</head>
<body>
	<jsp:include page="jsp/menu.jsp" />

	<div class="container">
		<h1>${user_name[0] }${user_name[1] }様のマイページ</h1>

		<div class="row">
			<h2>所持クーポン</h2>
		</div>
		<div class="row">
			<%--<table id = "edit">
		<tr>
			<td><h2><span style="border-bottom: solid 2px black;">登録情報　　　</span></h2></td>
			<td>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</td>
			<td><button onClick = "editMode()" class = "hidden" style = "display: block;">変更する</button></td>
		</tr>
	</table>--%>

			<div>
				<form action="edit" method="post">
					<table>
						<tr>
							<td>姓&emsp;<input type="text" name="lastName" class="b1" style="width: 50px;"
								value="${user_name[0]}" readonly="readonly"></td>
							<td>名&emsp;<input type="text" name="firstName" class="b1" style="width: 50px;"
								value="${user_name[1]}" readonly="readonly"></td>
						</tr>
						<tr>
							<td>メールアドレス</td>
							<td colspan="3"><input type="text" name="mail1" class="b1" value="${user_data.userId }"
								readonly="readonly"></td>
						</tr>
						<tr>
							<td class="edit" style="display: none">メールアドレス(確認)</td>
							<td colspan="3"><input type="text" name="mail2" class="edit" style="display: none"></td>
						</tr>
						<tr>
							<td>パスワード</td>
							<td colspan="3"><input type="password" name="pass1" class="b1" value="？？？？？？"
								readonly="readonly"></td>
						</tr>
						<tr>
							<td class="edit" style="display: none">パスワード(確認)</td>
							<td colspan="3"><input type="password" name="pass2" class="edit" style="display: none"></td>
						</tr>
						<tr>
							<td>郵便番号</td>
							<td colspan="3"><input type="text" name="postalCode" class="b1"
								value="${user_data.postalCode }" readonly="readonly"></td>
						</tr>
						<tr>
							<td>住所１</td>
							<td colspan="3"><input type="text" name="address1" class="b1"
								value="${user_data.address1 }" readonly="readonly"></td>
						</tr>
						<tr>
							<td>住所２</td>
							<td colspan="3"><input type="text" name="address2" class="b1"
								value="${user_data.address2 }" readonly="readonly"></td>
						</tr>
						<tr>
							<td>電話番号</td>
							<td colspan="3"><input type="text" name="tel" class="b1" value="${user_data.tel }"
								readonly="readonly"></td>
						</tr>
						<tr>
							<td>生年月日</td>
							<td colspan="3"><input type="date" name="birthday" class="b1"
								value="${user_data.birthday }" readonly="readonly"></td>
						</tr>
						<tr>
							<td>カード番号</td>
							<td colspan="3"><input type="text" name="cardNum" class="b1"
								value="${user_data.cardNumber }" readonly="readonly"></td>
						</tr>
						<tr>
							<td>カード名義</td>
							<td colspan="3"><input type="text" name="cardName" class="b1"
								value="${user_data.cardName }" readonly="readonly"></td>
						</tr>
						<tr>
							<td>有効期限</td>
							<td><input type="text" name="expirationM" class="b1" style="width: 30px;"
								value="${user_data.cardExpirationMonth }" readonly="readonly">(月)</td>
							<td><input type="text" name="expirationY" class="b1" style="width: 50px;"
								value="${user_data.cardExpirationYear }" readonly="readonly">(年)</td>
						</tr>
					</table>


					<table>
						<tr>
							<td><input type="submit" class="edit" name="ok" value="確定" style="display: none;"
								onclick="check();return false;"></td>
							<td><input type="submit" class="edit" name="cancel" value="キャンセル" style="display: none;"></td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>

	<jsp:include page="jsp/footer.jsp" />

</body>
</html>