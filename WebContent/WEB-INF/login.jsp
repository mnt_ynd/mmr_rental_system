<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ログイン</title>
<!-- bootstrap3 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<!-- jQuery 3.1 -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script>
<link href="http://cdn.phpoll.com/css/animate.css" rel="stylesheet">
<!-- CSS -->
<link href="css/loginStyle.css" rel="stylesheet">
<script type="text/javascript">
	function login() {
		if (fm.id.value == "" || fm.pass.value == "") {
			alert("ログインIDとパスワードを入力してください");
			return false;
		}
		return true;
	}
</script>
</head>
<body>
	<jsp:include page="jsp/menu.jsp" />
	<div class="container">
		<h1>お客様ログイン</h1>
		<div class="row">
			<div class="card card-container">
				<%
					if (null != session.getAttribute("error_message")) {
				%>
				<div style="color: red">
					<%=session.getAttribute("error_message")%></div>
				<%
					}
				%>
				<%
					session.removeAttribute("error_message");
				%>
				<form class="form-signin" name="fm" action="login" method="post">
					<span id="reauth-email" class="reauth-email">ログインID</span> <input type="text" id="inputEmail"
						name="id" class="form-control" placeholder="ログインID" required autofocus></input> <span
						id="inputPassword" class="reauth-email">パスワード</span> <input type="password" id="inputPassword"
						name="pass" class="form-control" placeholder="Password" required></input>
					<button class="btn btn-lg btn-primary btn-block btn-signin" type="submit"
						onClick="return login()">ログイン</button>
				</form>

				<br>
				<br>

				<table>
					<tr>
						<td>会員登録がお済みでないお客様はこちら</td>
						<td>
							<form action="addUser" method="POST">
								<input type="submit" value="会員登録">
							</form>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<jsp:include page="jsp/footer.jsp" />
</body>
</html>