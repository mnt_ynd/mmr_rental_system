<%@page import="jp.mnt.dto.CouponDTO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="
jp.mnt.dto.DeliveryTimeDTO,java.util.ArrayList,jp.mnt.dto.GenreDTO,jp.mnt.dto.ItemDTO,java.text.SimpleDateFormat, java.util.Date, java.text.DateFormat
,java.text.ParseException,java.util.Calendar
" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>お届け日時確認画面</title>
<!-- bootstrap3 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<!-- jQuery 3.1 -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script>
<link href="http://cdn.phpoll.com/css/animate.css" rel="stylesheet">

<!-- CSS -->
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<jsp:include page="menu.jsp" />
	<h1>お届け日時選択画面</h1>
	<br>
	<br>
	<div class="container">
	<form action="order" method="post" name="agenda">

		お届け日
			<%
			Calendar cal = (Calendar)session.getAttribute("cal");
			Calendar cal2 = (Calendar)session.getAttribute("cal");
			DateFormat df = (DateFormat)session.getAttribute("df");
			DateFormat df2 = (DateFormat)session.getAttribute("df2");

			%>
		<div class="form-control">
		<select name="deliveryDay">
			<option value="-6"><%= df.format(cal.getTime())%></option>
			<% cal.add(Calendar.DATE, 1);%>
			<option value="-5"><%= df.format(cal.getTime())%></option>
			<% cal.add(Calendar.DATE, 1);%>
			<option value="-4"><%= df.format(cal.getTime())%></option>
			<% cal.add(Calendar.DATE, 1);%>
			<option value="-3"><%= df.format(cal.getTime())%></option>
			<% cal.add(Calendar.DATE, 1);%>
			<option value="-2"><%= df.format(cal.getTime())%></option>
			<% cal.add(Calendar.DATE, 1);%>
			<option value="-1"><%= df.format(cal.getTime())%></option>
			<% cal.add(Calendar.DATE, 1);%>
			<option value="0"><%= df.format(cal.getTime())%></option>
		</select>
		</div>
		<br>
		お届け時間帯
		<div class="form-control">
		<select name="deliveryTime">


		<%for(DeliveryTimeDTO dto : (ArrayList<DeliveryTimeDTO>)session.getAttribute("deliveryItme")){%>

			<option value="<%=dto.getDeliveryTimeId()%>"><%= dto.getDeliveryTimeName()%></option>
		<%}%>
		</select>
		</div>
		<br>
		所持クーポン
		<%CouponDTO couponDto = (CouponDTO)session.getAttribute("couponDto"); %>
		<%=couponDto.getCouponName() %>
		<%
		boolean boolCoupon = false;

		for(int intList : (ArrayList<Integer>)session.getAttribute("cart_genre")){
			if(intList > 32){
				intList = intList - 9;
			}
			if(intList == (int)session.getAttribute("couponId")){
				boolCoupon = true;
			}

		}
		%>

		<div class="form-control">
		<select name="useCoupon">
			<option value="0">利用しない</option>
			<%if(boolCoupon == true) {%>
				<option value="1">利用する</option>
			<%} %>
		</select>
		</div>
		<br>
		<button type="submit" class="btn btn-success">注文を確定する</button>
	</form>
	</div>

<jsp:include page="footer.jsp" />
</body>
</html>