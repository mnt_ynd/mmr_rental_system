<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>注文受付完了画面</title>
<!-- bootstrap3 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<!-- jQuery 3.1 -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script>
<link href="http://cdn.phpoll.com/css/animate.css" rel="stylesheet">

<!-- CSS -->
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<jsp:include page="menu.jsp" />
	<br>
	<br>
	<br>
    <div class="container">
    <%
    String error = (String)request.getAttribute("error");
    if("true".equals(error)){
    %>
    	<h1>ご注文ありがとうございました。</h1>

    <%}else{ %>
    	<%//TODO %>
		<h1>予期せぬエラーが発生しました</h1>
	<%} %>
	<br>
	<%if((Integer)request.getAttribute("couponId") == 0 && "true".equals(error)){ %>
    	<form action="garapon" method="post" name="end">
			<button type="submit" class="btn btn-primary">がらポン</button>
		</form>
    <%} %>
	<form action="home" method="post" name="end">
		<button type="submit" class="btn btn-success">トップへ</button>
	</form>

	<%--セッションの削除 --%>
	<%
		session.removeAttribute("cart_list");
		session.removeAttribute("recommend_word");
		session.removeAttribute("cart_genre");
	%>

	</div>
	<jsp:include page="footer.jsp" />
</body>
</html>