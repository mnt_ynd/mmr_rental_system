<%@page import="java.sql.Timestamp"%>
<%@ page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page
	import="jp.mnt.dto.RentalDetailDTO,java.util.ArrayList,java.util.Date,java.util.Calendar, java.text.SimpleDateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-language" content="ja">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規会員登録 -MMRオンライン-</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>HOME</title>
<!-- bootstrap4 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<!-- jQuery 3.1 -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script>
<link href="http://cdn.phpoll.com/css/animate.css" rel="stylesheet">
<!-- 郵便番号 -->
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>

<!-- CSS -->
<link rel="stylesheet" type="text/css" href="css/style.css">
<script type="text/javascript">
	function check() {
		if(add.lastName.value == "" || add.firsName.value == "" || add.email.value == "" || add.password.value == "" || add.password2.value == "" || add.zip11.value == "" || add.addr11.value == "" || add.address2.value == "" || add.tel.value == "" || add.birthday.value == "" || add.creditCard.value == "" || add.name.value == "" || add.creditCard.value == ""){
			alert("未入力の欄があります");
			return false;
		}
		return true;
	}
</script>
</head>
<body>
	<jsp:include page="menu.jsp" />
	<div class="container">
		<h1>新規会員登録</h1>
		<form name="add" method="post" action="insertUser">
			<!-- 名前 -->
			<c:if test="${!empty requestScope.lastName }">
				<div style="color: red">
					<c:out value="${requestScope.lastName }" />
				</div>
			</c:if>
			<c:if test="${!empty requestScope.firstName }">
				<div style="color: red">
					<c:out value="${requestScope.firstName }" />
				</div>
			</c:if>
			<div class="form-group row">
				<label for="lastName" class="col-sm-2 form-control-label">姓</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" name="lastName" id="LastName"
						value="<%if(request.getAttribute("userDto") != null){ %><c:out value="${requestScope.userDto.lastName }"/><%}%>"
						placeholder="姓">
				</div>
				<label for="firstName"
					class="col-sm-2 form-control-label text-center">名</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" name="firsName" id="FirstName"
						value="<%if(request.getAttribute("userDto") != null){ %><c:out value="${requestScope.userDto.firstName }"/><%}%>"
						placeholder="名">
				</div>
			</div>
			<!-- メアド -->

			<c:if test="${!empty requestScope.email }">
				<div style="color: red">
					<c:out value="${requestScope.email }" />
				</div>
			</c:if>
			<div class="form-group row">
				<label for="Email" class="col-sm-2 form-control-label">メールアドレス</label>
				<div class="col-sm-5">
					<input type="text" name="email" id="Email"
						value="<%if(request.getAttribute("userDto") != null){ %><c:out value="${requestScope.userDto.userId }"/><%}%>"
						placeholder="メールアドレスを入力してください。" class="form-control">
				</div>
			</div>
			<!-- パスワード -->
			<c:if test="${!empty requestScope.password }">
				<div style="color: red">
					<c:out value="${requestScope.password }" />
				</div>
			</c:if>
			<div class="form-group row">
				<label for="password" class="col-sm-2 form-control-label">パスワード</label>
				<div class="col-sm-5">
					<input type="password" name="password" id="password"
						placeholder="パスワードを入力してください。" class="form-control">
				</div>
			</div>
			<!-- パスワード確認 -->

			<div class="form-group row">
				<label for="password2" class="col-sm-2 form-control-label">パスワード(確認)</label>
				<div class="col-sm-5">
					<input type="password" name="password2" id="password2"
						placeholder="パスワードを入力してください。" class="form-control">
				</div>
			</div>
			<!-- 郵便番号 -->
			<c:if test="${!empty requestScope.zip11 }">
				<div style="color: red">
					<c:out value="${requestScope.zip11}" />
				</div>
			</c:if>
			<div class="form-group row">
				<label for="zip11" class="col-sm-2 form-control-label">郵便番号</label>
				<div class="col-sm-3">
					<input type="text" name="zip11" id="zip11" size="10"
						onKeyUp="AjaxZip3.zip2addr(this,'','addr11','addr11');"
						value="<%if(request.getAttribute("userDto") != null){ %><c:out value="${requestScope.userDto.postalCode }"/><%}%>"
						placeholder="郵便番号を入力してください。" class="form-control">
				</div>
			</div>
			<!-- 住所１ -->
			<c:if test="${!empty requestScope.address }">
				<div style="color: red">
					<c:out value="${requestScope.address }" />
				</div>
			</c:if>
			<div class="form-group row">
				<label for="addr11" class="col-sm-2 form-control-label">住所１</label>
				<div class="col-sm-10">
					<input type="text" name="addr11" id="addr11"
						value="<%if(request.getAttribute("userDto") != null){ %><c:out value="${requestScope.userDto.address1 }"/><%}%>"
						placeholder="郵便番号を入力すると自動で挿入されます。" class="form-control">
				</div>
			</div>
			<!-- 住所２ -->
			<c:if test="${!empty requestScope.address2 }">
				<div style="color: red">
					<c:out value="${requestScope.address2 }" />
				</div>
			</c:if>
			<div class="form-group row">
				<label for="address2" class="col-sm-2 form-control-label">住所2</label>
				<div class="col-sm-10">
					<input type="text" name="address2" id="address2"
						value="<%if(request.getAttribute("userDto") != null){ %><c:out value="${requestScope.userDto.address2 }"/><%}%>"
						placeholder="市町村以降を入力してください。" class="form-control">
				</div>
			</div>
			<!-- 電話番号 -->
			<c:if test="${!empty requestScope.tel }">
				<div style="color: red">
					<c:out value="${requestScope.tel }" />
				</div>
			</c:if>
			<div class="form-group row">
				<label for="tel" class="col-sm-2 form-control-label">電話番号</label>
				<div class="col-sm-4">
					<input type="tel" name="tel" id="tel"
						value="<%if(request.getAttribute("userDto") != null){ %><c:out value="${requestScope.userDto.tel }"/><%}%>"
						placeholder="電話番号を入力してください。" class="form-control">
				</div>
			</div>
			<!-- 生年月日 -->
			<%
			Calendar cal = Calendar.getInstance();

			Date today = cal.getTime();

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String strToday = new SimpleDateFormat("yyyy-MM-dd").format(today);

			%>
			<c:if test="${!empty requestScope.birthday }">
				<div style="color: red">
					<c:out value="${requestScope.birthday }" />
				</div>
			</c:if>
			<div class="form-group row">
				<label for="birthday" class="col-sm-2 form-control-label">生年月日</label>
				<div class="col-sm-3">
					<input type="date" name="birthday" id="birthday"
						value="<%if(request.getAttribute("userDto") != null){ %><c:out value="${requestScope.userDto.birthday }"/><%}%>"
						class="form-control">
				</div>
			</div>
			<!-- カード番号 -->
			<c:if test="${!empty requestScope.creditCard }">
				<div style="color: red">
					<c:out value="${requestScope.creditCard }" />
				</div>
			</c:if>
			<div class="form-group row">
				<label for="creditCard" class="col-sm-2 form-control-label">カード番号</label>
				<div class="col-sm-4">
					<input type="text" name="creditCard" id="creditCard"
						value="<%if(request.getAttribute("userDto") != null){ %><c:out value="${requestScope.userDto.cardNumber }"/><%}%>"
						placeholder="カード番号を入力してください。" class="form-control">
				</div>
			</div>
			<!-- カード名義 -->
			<c:if test="${!empty requestScope.name }">
				<div style="color: red">
					<c:out value="${requestScope.name }" />
				</div>
			</c:if>
			<div class="form-group row">
				<label for="name" class="col-sm-2 form-control-label">お名前(ローマ字)</label>
				<div class="col-sm-5">
					<input type="text" name="name" id="name"
						value="<%if(request.getAttribute("userDto") != null){ %><c:out value="${requestScope.userDto.cardName }"/><%}%>"
						placeholder="お名前(ローマ字)を入力してください。" class="form-control">
				</div>
			</div>
			<!-- 有効期限 -->
			<c:if test="${!empty requestScope.expiration }">
				<div style="color: red">
					<c:out value="${requestScope.expiration }" />
				</div>
			</c:if>
			<div class="form-group row">
				<label for="expiration" class="col-sm-2 form-control-label">有効期限</label>
				<div class="col-sm-3">
					<select class="form-control" name="month">
						<%if(request.getAttribute("userDto") != null){ %>
							<option value="<c:out value="${requestScope.userDto.cardExpirationMonth }"/>"><c:out value="${requestScope.userDto.cardExpirationMonth }"/>月</option>
						<%}%>

						<option value="01">01月</option>
						<option value="02">02月</option>
						<option value="03">03月</option>
						<option value="04">04月</option>
						<option value="05">05月</option>
						<option value="06">06月</option>
						<option value="07">07月</option>
						<option value="08">08月</option>
						<option value="09">09月</option>
						<option value="10">10月</option>
						<option value="11">11月</option>
						<option value="12">12月</option>
					</select>
				</div>
				<div class="col-sm-3">
					<select class="form-control" name="year">
						<%if(request.getAttribute("userDto") != null){ %>
							<option value="<c:out value="${requestScope.userDto.cardExpirationYear }"/>"><c:out value="${requestScope.userDto.cardExpirationYear }"/>月</option>
						<%}%>
						<%
							Calendar c = Calendar.getInstance();
							for( int i = 0; i <= 9; i++ ){
							String s = String.format( "%tY", c, c );
							c.add( Calendar.YEAR , 1 );
						%>
						<option value="<%= s %>"><%= s %>年</option>
						<% } %>
					</select>
				</div>
			</div>
			<input type="submit" value="送信" class="btn btn-primary" onclick="return check()"> <input
				type="reset" value="キャンセル" class="btn btn-default" onClick = "history.back()">
		</form>
	</div>

	<jsp:include page="footer.jsp" />
</body>
</html>