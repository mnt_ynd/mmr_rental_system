<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>がらポン画面</title>
<!-- bootstrap4 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<!-- jQuery 3.1 -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script>
<link href="http://cdn.phpoll.com/css/animate.css" rel="stylesheet">
<!-- 郵便番号 -->
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>

<!-- CSS -->
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<jsp:include page="menu.jsp" />
	<div class="container">
	<div class="center-block">



	<c:if test="${!empty couponImg}">
		<img src="image/coupon/<c:out value="${couponImg}" ></c:out>"  title="garapon">
	</c:if>
	<c:if test="${!empty haveCoupon}">
		<c:out value="${haveCoupon }"></c:out>
	</c:if>
	<br>
	<form action="home" method="post" name="end">
		<button type="submit" class="btn btn-success">トップへ</button>
	</form>
	</div>
	</div>
	<jsp:include page="footer.jsp" />
</body>
</html>