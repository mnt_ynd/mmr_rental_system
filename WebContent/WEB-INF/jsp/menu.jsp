<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- jQuery 3.1 -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script>
<link href="http://cdn.phpoll.com/css/animate.css" rel="stylesheet">

<!-- bootstrap4 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<!-- Webフォント -->
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet">
<body>
	<nav class="navbar navbar-toggleable-md navbar-light bg-faded">

		<button class="navbar-toggler navbar-toggler-right" type="button"
			data-toggle="collapse" data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<a class="navbar-brand" href="#" onClick="document.home.submit(); return false;" >
		<img alt="" src="image/logo.png" width="200px"></img></a>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">

			<% if(session.getAttribute("login") != null){%>
				<li class="nav-item"><a class="nav-link" href="#" onClick="document.logout.submit(); return false;"><i
						class="fa fa-sign-out"> ログアウト</i></a></li>
			<%}%>

				<% String path = request.getServletPath();
				if(!"/WEB-INF/login.jsp".equals(path)){%>
					<li class="nav-item"><a class="nav-link" href="#" onClick="document.headercart.submit();">
					<i class="fa fa-shopping-bag"> カート</i><span class="badge badge-default"><c:out value="${sessionScope.cart_list.size()}" /></span></a></li>
				<% }%>

				<% if(session.getAttribute("login") == null && !"/WEB-INF/login.jsp".equals(path)){%>
					<li class="nav-item"><a class="nav-link" href="#" onClick="document.add.submit();">
					<i class="fa fa-address-card"> 会員登録</i></a></li>
				<% }%>

				<%if(session.getAttribute("login") != null){%>
					<%--<li class="nav-item"><a class="nav-link" href="#"><i
							class="fa fa-info-circle"> 注文履歴</i></a></li>--%>
					<li class="nav-item"><a class="nav-link" href="#" onClick="document.garapon.submit(); return false;"><i
							class="fa fa-heart"> ガラポン</i></a></li>
					<li class="nav-item"><a class="nav-link" href="#" onClick="document.mypage.submit(); return false;">
					<i class="fa fa-user-circle"> マイページ</i></a></li>
				<%}%>
			</ul>

			<% if(session.getAttribute("login") == null && !"/WEB-INF/login.jsp".equals(path)){%>
				<form class="form-inline my-2 my-lg-0" action = "login" method = "POST">
					<input class="form-control mr-sm-2" type="text" name = "id" placeholder="メールアドレス">
					<input class="form-control mr-sm-2" type="password" name =  "pass" placeholder="パスワード">
					<button class="btn btn-outline-success my-2 my-sm-0" onClick="return login()" type="submit">ログイン</button>
				</form>
			<% }else if(session.getAttribute("login") != null && !"/WEB-INF/login.jsp".equals(path)){%>
				 ようこそ${ user_name[0]}様
			<% }%>

			<form name = "home" action = "home" method = "POST">
			</form>

			<form name = "mypage" action = "mypage" method = "POST">
			</form>

			<form name = "headercart" action = "cart" method = "POST">
				<input type = "hidden" name = "head" value = "head">
			</form>

			<form name = "garapon" action = "garapon" method = "POST">
			</form>

			<form name = "logout" action = "logout" method = "POST">
			</form>

			<form name = "add" action = "addUser" method = "POST">
			</form>
		</div>
	</nav>
</body>
</html>