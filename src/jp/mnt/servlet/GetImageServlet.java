package jp.mnt.servlet;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.mnt.dao.ItemDetailDAO;

/**
 * Servlet implementation class GetImageServlet
 */
@WebServlet("/getimage")
public class GetImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		ItemDetailDAO dao = new ItemDetailDAO();

		String numStr = req.getParameter("id");
		int itemId = Integer.parseInt(numStr);
		BufferedImage img = dao.selectImageById(itemId);

		// 画像をクライアントに返却する
		res.setContentType("image/jpeg");
		OutputStream os = res.getOutputStream();
		if (img != null) {
			ImageIO.write(img, "jpg", os);
		}
		os.flush();
	}
}
