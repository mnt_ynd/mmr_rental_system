package jp.mnt.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Random;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.dao.ConnectionManager;
import jp.mnt.dao.CouponDistributionSelectDAO;

/**
 * Servlet implementation class GaraponSOervlet
 */
@WebServlet("/garapon")
public class GaraponServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("login.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(false);

		if ("".equals(session.getAttribute("login"))) {
			doGet(request, response);
			return;
		}
		int index = (int) session.getAttribute("user_index");
		int couponId = 0;

		try (Connection conn = ConnectionManager.getConnection()) {

			CouponDistributionSelectDAO couponDistributionSelectDao = new CouponDistributionSelectDAO(conn);
			// 所持クーポンIDの取得
			couponId = couponDistributionSelectDao.haveCoupon(index);

			if (couponId == 0) {
				conn.setAutoCommit(false);

				Random rnd = new Random();
				int iValue = rnd.nextInt(32) + 1;
				String img = "coupon_" + iValue + ".gif";

				int flg = couponDistributionSelectDao.insertCoupon(index, iValue);
				// 正常にinsertできたか判別しコミット
				if (flg == 1) {
					conn.commit();
					request.setAttribute("couponImg", img);

				} else {
					conn.rollback();
					request.setAttribute("haveCoupon", "エラーが発生しました");
				}

				request.getRequestDispatcher("/WEB-INF/jsp/garapon.jsp").forward(request, response);

			} else {
				request.setAttribute("haveCoupon", "すでにクーポンを所持しています");
				request.getRequestDispatcher("/WEB-INF/jsp/garapon.jsp").forward(request, response);
			}

		} catch (NamingException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
