package jp.mnt.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.dao.ConnectionManager;
import jp.mnt.dao.HomeDAO;
import jp.mnt.dao.ItemDetailDAO;
import jp.mnt.dao.SearchDAO;
import jp.mnt.dto.CategoryDTO;
import jp.mnt.dto.GenreDTO;
import jp.mnt.dto.ItemDTO;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet("/home")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// セッション
		HttpSession session = req.getSession(true);

		// 変数宣言
		String nextPage = null;
		ArrayList<Integer> cartList = new ArrayList<Integer>();

		makeList(session);

		// セッションに保存
		session.setAttribute("cart_list", cartList);

		nextPage = "/WEB-INF/home.jsp";
		RequestDispatcher rd = req.getRequestDispatcher(nextPage);
		rd.forward(req, res);

	}

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		try {
			// セッション
			HttpSession session = req.getSession(false);

			// 変数宣言
			String nextPage = null;

			makeList(session);

			// カートのセッション情報がなければ作成する
			if (session.getAttribute("cart_list") == null) {
				ArrayList<Integer> cartList = new ArrayList<Integer>();
				// セッションに保存
				session.setAttribute("cart_list", cartList);
			}

			nextPage = "/WEB-INF/home.jsp";
			RequestDispatcher rd = req.getRequestDispatcher(nextPage);
			rd.forward(req, res);
		} catch (NullPointerException e) {
			res.sendRedirect("home");
			return;
		}
	}

	public static void makeList(HttpSession session) {

		try (Connection conn = ConnectionManager.getConnection()) {

			// 変数宣言
			ArrayList<String[]> wordList = new ArrayList<String[]>();
			String[] words = new String[3];

			// カテゴリ、ジャンルリスト
			SearchDAO dao = new SearchDAO();
			ArrayList<CategoryDTO> categoryList = dao.getCategoryList();
			ArrayList<GenreDTO> genreList = dao.getGenreList();

			// おススメ商品を取得する
			ArrayList<ItemDTO> itemList = new ArrayList<ItemDTO>();
			HomeDAO homeDao = new HomeDAO(conn);
			itemList = homeDao.recommendList();

			// 商品情報リストを作成
			for (int i = 0; i < itemList.size(); ++i) {
				ItemDetailDAO itemDao = new ItemDetailDAO();

				// カテゴリ、新旧区分、ジャンルを変換する
				String category = itemDao.getCategoryName(itemList.get(i).getCategoryId());
				String genre = itemDao.getGenreName(itemList.get(i).getGenreId());
				String newAndOld = itemDao.getNewAndOldName(itemList.get(i).getNewAndOldId());

				words[0] = category;
				words[1] = genre;
				words[2] = newAndOld;

				wordList.add(words);
			}

			// セッションに保存
			session.setAttribute("category_list", categoryList);
			session.setAttribute("genre_list", genreList);
			session.setAttribute("item_list", itemList);
			session.setAttribute("recommend_word", wordList);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
