package jp.mnt.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.dao.ClientRentalDAO;
import jp.mnt.dao.ConnectionManager;
import jp.mnt.dao.CouponDistributionSelectDAO;
import jp.mnt.dto.CouponDTO;
import jp.mnt.dto.ItemDTO;
import jp.mnt.dto.RentalDTO;

/**
 * Servlet implementation class OrderServlet
 */
@WebServlet("/order")
public class OrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("login.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(false);

		if ("".equals(session.getAttribute("login"))) {
			doGet(request, response);
			return;
		}

		// データをrentalDtoに格納
		RentalDTO rentalDto = new RentalDTO();
		String str = (String) request.getParameter("deliveryDay");

		// 受け取った値からお届け日を変更
		Calendar cal = (Calendar) session.getAttribute("cal");
		cal.add(Calendar.DATE, Integer.parseInt(str));
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		rentalDto.setDeliveryDate(df.format(cal.getTime()));

		String deliveryTime = (String) request.getParameter("deliveryTime");
		rentalDto.setDeliveryTimeId(deliveryTime);
		String useCoupon = (String) request.getParameter("useCoupon");
		CouponDTO couponDto = (CouponDTO) session.getAttribute("couponDto");
		int couponId = (Integer) session.getAttribute("couponId");
		int couponIndex;
		int discount = couponDto.getDiscount();
		int userIndex = (int) session.getAttribute("user_index");
		rentalDto.setDiscount(Integer.toString(discount));
		rentalDto.setUserIndex(Integer.toString(userIndex));

		// カート情報を持ってくる
		// ArrayList<Integer> cartList =
		// (ArrayList<Integer>)session.getAttribute("cart_list");
		ArrayList<ItemDTO> itemDTO = (ArrayList<ItemDTO>) session.getAttribute("cart_item");

		try (Connection conn = ConnectionManager.getConnection()) {

			conn.setAutoCommit(false);

			int conFlgInt = 0;
			boolean conFlgBoolean = false;

			// ユーザーIDからcouponIndexの取得
			ClientRentalDAO dao = new ClientRentalDAO(conn);
			CouponDistributionSelectDAO couponDao = new CouponDistributionSelectDAO(conn);
			couponIndex = couponDao.couponIndex(userIndex);
			rentalDto.setCouponIndex(Integer.toString(couponIndex));

			// 変数
			int rentalNumber = 0;
			int result = 0;
			int useCouponResult = 0;
			// クーポンを使うか判別し、rentalテーブルにinsert
			if ("1".equals(useCoupon)) {
				// クーポンを使うレンタルメソッド
				rentalNumber = dao.insertAll(rentalDto);
				// System.out.println("受付番号"+rentalNumber);

				// 商品を貸し出す
				result = dao.rentalItem(itemDTO, rentalNumber);
				// System.out.println("受付番号"+rentalNumber);

				// 使用したクーポンをtrueに変更
				useCouponResult = couponDao.useCoupon(couponId);
				// System.out.println(useCouponResult);
				if (rentalNumber >= 1 && result >= 1 && useCouponResult == 1) {
					conFlgBoolean = true;
				}
			} else {
				// クーポンを使わないレンタルメソッド
				rentalNumber = dao.insert(rentalDto);
				// System.out.println("受付番号"+rentalNumber);
				// 商品を貸し出す
				result = dao.rentalItem(itemDTO, rentalNumber);
				// System.out.println("受付番号"+rentalNumber);
				if (rentalNumber >= 1 && result >= 1) {
					conFlgBoolean = true;
				}
			}

			// 正常にinsertできたか判別しコミット
			if (conFlgBoolean == true) {
				// System.out.println("レンタル完了");
				conn.commit();
				conn.setAutoCommit(true);
				// カートデータを削除
				session.removeAttribute("cart_list");
				// session.removeAttribute("item_list");
				session.removeAttribute("recommend_word");
				request.setAttribute("error", "true");
			} else {
				// System.out.println("レンタル未完了");
				conn.rollback();
				conn.setAutoCommit(true);
				request.setAttribute("error", "false");
			}

			CouponDistributionSelectDAO couponDistributionSelectDao = new CouponDistributionSelectDAO(conn);
			// 所持クーポンIDの取得
			couponId = couponDistributionSelectDao.haveCoupon(userIndex);
			request.setAttribute("couponId", couponId);

			request.getRequestDispatcher("/WEB-INF/jsp/completion.jsp").forward(request, response);

		} catch (NamingException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
