package jp.mnt.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.dao.ConnectionManager;
import jp.mnt.dao.CouponDistributionSelectDAO;
import jp.mnt.dao.DeliveryItmeDAO;
import jp.mnt.dto.CouponDTO;
import jp.mnt.dto.DeliveryTimeDTO;

/**
 * Servlet implementation class AgendeServlet
 */
@WebServlet("/agenda")
public class AgendaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.sendRedirect("/WEB-INF/login.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(false);

		//ログインしていなければログイン画面へ遷移する
		if(session.getAttribute("login") == null){
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/login.jsp");
			rd.forward(request, response);
			return;
		}

		int index = (int) session.getAttribute("user_index");

		try (Connection conn = ConnectionManager.getConnection()) {

			ArrayList<DeliveryTimeDTO> deliveryTimeList;
			int couponId = 0;
			DeliveryItmeDAO deliveryItmeDao = new DeliveryItmeDAO(conn);
			CouponDistributionSelectDAO couponDistributionSelectDao = new CouponDistributionSelectDAO(conn);

			// お届け日時データの取得
			deliveryTimeList = deliveryItmeDao.selectAll();
			// 所持クーポンIDの取得
			couponId = couponDistributionSelectDao.haveCoupon(index);

			// クーポンを所持していたらクーポンIDからクーポン名を取得
			CouponDTO couponDto = new CouponDTO();
			if (couponId != 0) {
				couponDto = couponDistributionSelectDao.selectCouponName(couponId);
			}else {
				couponDto.setCouponName("無し");
				couponDto.setDiscount(0);
			}


			// DateFormat作成
			DateFormat df = new SimpleDateFormat("M月d日");
			DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");


			// 今日を取得
			Date dt = new Date();

			// カレンダークラスのインスタンスを取得
			Calendar cal = Calendar.getInstance();
			// 現在時刻を設定
			cal.setTime(dt);

			cal.add(Calendar.DATE, 1);

			ArrayList<Integer> intList = new ArrayList<Integer>() ;
			ArrayList<Integer> intList2 = new ArrayList<Integer>();

			//カート内のジャンルサンプル
			intList.add(26);
			intList.add(35);
			session.setAttribute("intList", intList);
			//カート内の商品IDサンプル
			intList2.add(1);
			intList2.add(4);
			session.setAttribute("intList2", intList2);


			// クーポン情報とお届け日時データを送る
			session.setAttribute("couponId", couponId);
			session.setAttribute("df", df);
			session.setAttribute("df2", df2);
			session.setAttribute("dt", dt);
			session.setAttribute("cal", cal);
			session.setAttribute("couponDto", couponDto);
			session.setAttribute("deliveryItme", deliveryTimeList);

			request.getRequestDispatcher("/WEB-INF/jsp/agenda.jsp").forward(request, response);

		} catch (NamingException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
