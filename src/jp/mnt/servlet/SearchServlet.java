package jp.mnt.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.dao.SearchDAO;
import jp.mnt.dto.ItemDTO;

@WebServlet("/search")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.sendRedirect("home");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		// セッションの取得
		HttpSession session = req.getSession(false);

		req.setCharacterEncoding("UTF-8");

		// 変数定義
		ArrayList<ItemDTO> itemList = new ArrayList<ItemDTO>();
		SearchDAO dao = new SearchDAO();
		String nextPage = "/WEB-INF/home.jsp";

		// 入力された検索項目を取得
		String searchString = req.getParameter("item_");
		String category = req.getParameter("category");
		String genre = req.getParameter("genre");
		try {
			if (genre == null) {
				genre = "0";
			}
			// キャスト
			int categoryId = Integer.parseInt(category);
			int genreId = Integer.parseInt(genre);

			// 入力されたキーワードをチェックする
			String message = "";
			// 入力や選択の有無での処理の場合分け
			if ("0".equals(category) && "0".equals(genre) && "".equals(searchString)) {
				itemList = dao.searchAllItem();
			} else if ("0".equals(category) && "".equals(searchString)) {
				itemList = dao.searchItem(genreId);
			} else if ("0".equals(genre) && "".equals(searchString)) {
				itemList = dao.searchItem(category);
			} else if ("0".equals(category) && "0".equals(genre)) {
				itemList = dao.searchKeyword(searchString);
			} else if ("".equals(searchString)) {
				itemList = dao.searchItem(categoryId, genreId);
			} else if ("0".equals(category)) {
				itemList = dao.searchItem(genreId, searchString);
			} else if ("0".equals(genre)) {
				itemList = dao.searchItem(searchString, categoryId);
			} else {
				itemList = dao.searchItem(searchString, categoryId, genreId);
			}

			// 検索結果が0件の場合
			if (itemList.size() == 0) {
				message = "該当する商品はありません。";
			}
			req.setAttribute("message", message);

			// 見つかった情報をリストで送る
			session.setAttribute("item_list", itemList);

			nextPage = "/WEB-INF/searchpage.jsp";
			RequestDispatcher rd = req.getRequestDispatcher(nextPage);
			rd.forward(req, res);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
