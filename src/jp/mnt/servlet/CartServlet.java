//株式会社ソフトワン　峰勇気
package jp.mnt.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.dao.ItemDetailDAO;
import jp.mnt.dto.ItemDTO;

/**
 * Servlet implementation class CartServlet
 */
@WebServlet("/cart")
public class CartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		HttpSession session = req.getSession(false);

		if (session != null) {
			session.invalidate();
		}

		HomeServlet.makeList(session);

		RequestDispatcher rd = req.getRequestDispatcher("WEB-INF/home.jsp");
		rd.forward(req, res);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// セッション取得
		HttpSession session = req.getSession(false);

		// 変数宣言
		String nextPage = "/WEB-INF/home.jsp";
		ArrayList<Integer> cartList = new ArrayList<Integer>();
		ArrayList<ItemDTO> cartItem = new ArrayList<ItemDTO>();
		ArrayList<String[]> wordList = new ArrayList<String[]>();
		ArrayList<Integer> genreList = new ArrayList<Integer>();
		String[] words = new String[3];

		// セッションに保存してあるカート情報を取得
		cartList = (ArrayList<Integer>) session.getAttribute("cart_list");

		// ボタンを押された商品情報を取得
		String idStr = req.getParameter("item");
		String head = req.getParameter("head");

		// ヘッダーのアイコンからアクセスがあった場合
		if (head != null && idStr == null) {
			// 商品情報リストを作成
			for (int i = 0; i < cartList.size(); ++i) {
				ItemDetailDAO dao = new ItemDetailDAO();
				ItemDTO dto = dao.selectItem(cartList.get(i));

				// カテゴリ、新旧区分、ジャンルを変換する
				String category = dao.getCategoryName(dto.getCategoryId());
				String genre = dao.getGenreName(dto.getGenreId());
				String newAndOld = dao.getNewAndOldName(dto.getNewAndOldId());

				words[0] = category;
				words[1] = genre;
				words[2] = newAndOld;

				genreList.add(dto.getGenreId());
				wordList.add(words);
				cartItem.add(dto);
			}

			// リストをセッションに保存する
			session.setAttribute("cart_list", cartList);
			session.setAttribute("cart_item", cartItem);
			session.setAttribute("word_list", wordList);
			session.setAttribute("cart_genre", genreList);

			nextPage = "/WEB-INF/cart.jsp";
		} else {
			try {
				int id = Integer.parseInt(idStr);
				// 取得した情報をリストに格納
				cartList.add(id);

				// 商品情報リストを作成
				for (int i = 0; i < cartList.size(); ++i) {
					ItemDetailDAO dao = new ItemDetailDAO();
					ItemDTO dto = dao.selectItem(cartList.get(i));

					// カテゴリ、新旧区分、ジャンルを変換する
					String category = dao.getCategoryName(dto.getCategoryId());
					String genre = dao.getGenreName(dto.getGenreId());
					String newAndOld = dao.getNewAndOldName(dto.getNewAndOldId());

					words[0] = category;
					words[1] = genre;
					words[2] = newAndOld;

					genreList.add(dto.getGenreId());
					wordList.add(words);
					cartItem.add(dto);
				}

				// リストをセッションに保存する
				session.setAttribute("cart_list", cartList);
				session.setAttribute("cart_item", cartItem);
				session.setAttribute("word_list", wordList);
				session.setAttribute("cart_genre", genreList);

				nextPage = "/WEB-INF/cart.jsp";
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}

		RequestDispatcher rd = req.getRequestDispatcher(nextPage);
		rd.forward(req, res);
	}
}
