package jp.mnt.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.dao.ConnectionManager;
import jp.mnt.dao.UserDAO;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/login.jsp");
		rd.forward(req, res);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		String nextPage = null;
		// 入力されたログインIDとパスワードの取得
		String id = req.getParameter("id");
		String pw = req.getParameter("pass");
		HttpSession session = req.getSession(false);

		//セッションがない場合セッションを生成
		if(session == null){
			session = req.getSession(true);
		}

		// 入力チェックを行い不適切だとerror_messageをlogin.jspに送る
		String check = loginCheck(id, pw);
		if (!check.equals("")) {
			session.setAttribute("error_message", check);
			doGet(req, res);
			return;
		}

		// コネクションの取得
		Connection conn = null;

		try {
			conn = ConnectionManager.getConnection();

			// idとpasswordの検索
			UserDAO dao = new UserDAO(conn);
			boolean selectByIdAndPassword = false;
			selectByIdAndPassword = dao.selectByIdAndPassword(id, pw);

			//nullの時error_messageをlogiin.jspに送る
			if (selectByIdAndPassword == false) {
				session.setAttribute("error_message", "ユーザIDまたはパスワードが間違っています");
				doGet(req, res);
				return;
			}

			// nullでない時error_messageをリセット
			session.removeAttribute("error_message");

			//姓名を取得
			String[] str = new String[2];
			str = dao.getName(id, pw);

			//ユーザインデックスの取得
			int index = dao.getIndex(id, pw);

			// セッションにユーザーインデックスと姓名、loginという文字をセット
			session.setAttribute("login", "login");
			session.setAttribute("user_name", str);
			session.setAttribute("user_index", index);

			HomeServlet.makeList(session);

			// 遷移先を決める
			nextPage = "/WEB-INF/home.jsp";


			RequestDispatcher rd = req.getRequestDispatcher(nextPage);
			rd.forward(req, res);

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}


	}

	/**
	 * ログインIDとパスワードの入力チェックを行う
	 */
	public String loginCheck(String id, String pass) {

		String check = "";
		String matchMail = "([a-zA-Z0-9][a-zA-Z0-9_.+\\-]*)@(([a-zA-Z0-9][a-zA-Z0-9_\\-]+\\.)+[a-zA-Z]{2,6})";
		// idが半角英数字以外があるとエラー文を返す
		if(!id.matches(matchMail)){
			check = "ログインIDまたはパスワードが間違っています。";
			return check;
		}

		// passに半角英数字以外があるとエラー文を返す
		for (int i = 0; i < pass.length(); i++) {
			char c = pass.charAt(i);
			if ((c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z')) {
				check = "ログインIDまたはパスワードが間違っています。";
				return check;
			}
		}
		return check;
	}

}
