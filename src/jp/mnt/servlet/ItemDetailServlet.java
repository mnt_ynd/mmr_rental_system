package jp.mnt.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.dao.ItemDetailDAO;
import jp.mnt.dto.ItemDTO;

/**
 * Servlet implementation class ItemDetailServlet
 */
@WebServlet("/detail")
public class ItemDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession session = req.getSession(false);

		String nextPage = null;

		if (session != null) {
			session.invalidate();
		}

		// 遷移先は未ログインのホーム画面
		nextPage = "/WEB-INF/home.jsp";

		RequestDispatcher rd = req.getRequestDispatcher(nextPage);
		rd.forward(req, res);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// 変数宣言
		String nextPage = null;
		ItemDetailDAO dao = new ItemDetailDAO();
		HttpSession session = req.getSession(false);

		// 入力された商品IDを取得する
		String numStr = req.getParameter("detail");

		try {
			int itemId = Integer.parseInt(numStr);

			if (itemId > 0) {
				// 商品データを取得
				ItemDTO dto = dao.selectItem(itemId);

				// カテゴリ、新旧区分、ジャンルを変換する
				String category = dao.getCategoryName(dto.getCategoryId());
				String genre = dao.getGenreName(dto.getGenreId());
				String newAndOld = dao.getNewAndOldName(dto.getNewAndOldId());

				String[] words = new String[3];
				words[0] = category;
				words[1] = genre;
				words[2] = newAndOld;
				// セッションに保存
				session.setAttribute("item", dto);
				session.setAttribute("words", words);
				nextPage = "/WEB-INF/itemdetail.jsp";
			} else {
				// エラー画面に遷移
				nextPage = "/WEB-INF/test.jsp";
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
			// エラー画面に遷移
		}

		RequestDispatcher rd = req.getRequestDispatcher(nextPage);
		rd.forward(req, res);
	}

}
