package jp.mnt.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LogoutServlet
 */
@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		//セッション取得
		HttpSession session = req.getSession(false);

		//セッションに保存してあるユーザ情報を削除する。
		session.removeAttribute("login");
		session.removeAttribute("user_id");
		session.removeAttribute("user_index");

		HomeServlet.makeList(session);

		//未ログインのホームへのパスを設定
		String nextPage = "/WEB-INF/home.jsp";
		RequestDispatcher rd = req.getRequestDispatcher(nextPage);
		rd.forward(req, res);

	}

}
