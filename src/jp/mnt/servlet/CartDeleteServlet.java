package jp.mnt.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.dto.ItemDTO;

/**
 * Servlet implementation class CartDeleteServlet
 */
@WebServlet("/cartdelete")
public class CartDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		//セッション取得
		HttpSession session = req.getSession(false);

		//変数宣言
		String nextPage = "/WEB-INF/home.jsp";
		ArrayList<Integer> cartList = new ArrayList<Integer>();
		ArrayList<ItemDTO> cartItem = new ArrayList<ItemDTO>();
		ArrayList<String[]> wordList = new ArrayList<String[]>();

		//セッションに保存してあるカート情報を取得
		cartList = (ArrayList<Integer>)session.getAttribute("cart_list");
		cartItem = (ArrayList<ItemDTO>)session.getAttribute("cart_item");
		wordList = (ArrayList<String[]>)session.getAttribute("word_list");

		//ボタンを押された商品情報を取得
		String idStr = req.getParameter("item");

		try{
			int id = Integer.parseInt(idStr);

			//削除対象の商品を探してリストから外す
			for(int i = 0; i < cartList.size();++i){
				if(cartList.get(i) == id){
					//一致した商品の情報を削除する
					cartList.remove(i);
					cartItem.remove(i);
					wordList.remove(i);
					break;
				}
			}

			//リストをセッションに保存する
			session.setAttribute("cart_list", cartList);
			session.setAttribute("cart_item", cartItem);
			session.setAttribute("word_list", wordList);

			nextPage = "/WEB-INF/cart.jsp";
		}
		catch(NumberFormatException e){
			e.printStackTrace();
		}

		RequestDispatcher rd = req.getRequestDispatcher(nextPage);
		rd.forward(req, res);
	}

}
