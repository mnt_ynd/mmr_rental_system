package jp.mnt.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.dao.MyPageDAO;
import jp.mnt.dto.UserDTO;

/**
 * Servlet implementation class MyPageServlet
 */
@WebServlet("/mypage")
public class MyPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession session = req.getSession(false);
		String nextPage = null;
		// if (session != null) {
		// session.invalidate();
		// }

		// 遷移先は未ログインのホーム画面
		nextPage = "home";

		RequestDispatcher rd = req.getRequestDispatcher(nextPage);
		rd.forward(req, res);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// 変数宣言
		String nextPage = null;
		MyPageDAO dao = new MyPageDAO();

		// セッションの取得
		HttpSession session = req.getSession(false);

		// ユーザー情報の取得
		int index = (int) session.getAttribute("user_index");

		// 該当顧客がいるかをチェックする
		boolean check = dao.checkIndex(index);

		if (check) {
			// 該当顧客がいるなら、顧客データを取得して、セッションに保存する。
			UserDTO dto = dao.getData(index);
			session.setAttribute("user_data", dto);

			nextPage = "/WEB-INF/mypage.jsp";
		} else {
			session.invalidate();
			nextPage = "/WEB-INF/home.jsp";
		}
		// userIndex = session.getAttribute("user_index");

		RequestDispatcher rd = req.getRequestDispatcher(nextPage);
		rd.forward(req, res);
	}

}
