package jp.mnt.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class EditServlet
 */
@WebServlet("/edit")
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");

		//変数宣言
		String nextPage = "/WEB-INF/home.jsp";
		String okAction = req.getParameter("ok");
		String cancelAction = req.getParameter("cancel");

		//セッションの取得
		HttpSession session = req.getSession(false);

		//押されたボタンで処理を変える。
		if(okAction != null && cancelAction == null){
			//登録情報を変更する
			//値の取得
			String[] data = new String[15];
			data[0] = req.getParameter("lastName");
			data[1] = req.getParameter("firstName");
			data[2] = req.getParameter("mail1");
			data[3] = req.getParameter("mail2");
			data[4] = req.getParameter("pass1");
			data[5] = req.getParameter("pass2");
			data[6] = req.getParameter("postalCode");
			data[7] = req.getParameter("address1");
			data[8] = req.getParameter("address2");
			data[9] = req.getParameter("tel");
			data[10] = req.getParameter("birthday");
			data[11] = req.getParameter("cardNum");
			data[12] = req.getParameter("cardName");
			data[13] = req.getParameter("expirationM");
			data[14] = req.getParameter("expirationY");
			for(int i = 0; i < data.length;i++){
//				System.out.println(data[i]);
			}

			//入力チェック
			//dataCheck(data);
			nextPage = "/WEB-INF/test2.jsp";

		}else if(okAction == null && cancelAction != null){
			//登録情報を変更しない

			//遷移先のアドレス
			nextPage = "/WEB-INF/test2.jsp";

		}else if(okAction != null && cancelAction != null){
			//エラー(ボタンが両方押された)
//			System.out.println("エラー");
		}else{
			//ボタンが押されていないのに遷移した。

		}

		//遷移先を決定する
		RequestDispatcher rd = req.getRequestDispatcher(nextPage);
		rd.forward(req, res);
	}

	/*入力チェック1
	 * 記号が入力されていないか
	 * ただし、メールアドレスのみ@が可
	 */
	public String dataCheck(String[] data) {
		String check = "";
		String matchMail = "([a-zA-Z0-9][a-zA-Z0-9_.+\\-]*)@(([a-zA-Z0-9][a-zA-Z0-9_\\-]+\\.)+[a-zA-Z]{2,6})";

		//未入力のチェック
		for(int i = 0; i < data.length;++i){
			if("".equals(data[i])){

			}
		}
		//半角英数字以外があるとエラー文を返す
//		for (int i = 0; i < data.length; ++i) {
//			//すべての項目が入力されているか
//			if(data[i] == null || "".equals(data[i])){
//				check = "未入力の項目があります。すべて入力してください。";
//				System.out.println(check);
//				return check;
//			}
//			//メールアドレスの入力チェック
//			if(i==2 || i==3){
//				if(!data[i].matches(matchMail)){
//					check = "入力禁止文字が含まれています。もう一度ご確認の上確定ボタンを押してください。";
//					return check;
//				}
//			}else{
//				// それ以外の入力チェック
//				for (int j = 0; j < data[i].length(); ++j) {
//					char c = data[i].charAt(j);
//					if ((c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z')) {
//						check = "ログインIDまたはパスワードが間違っています。";
//						return check;
//					}
//				}
//			}
//		}
		return check;
	}

}
