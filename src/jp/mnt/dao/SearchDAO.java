package jp.mnt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Normalizer;
import java.util.ArrayList;

import javax.naming.NamingException;

import jp.mnt.dto.CategoryDTO;
import jp.mnt.dto.GenreDTO;
import jp.mnt.dto.ItemDTO;

public class SearchDAO {

	// カテゴリリストの作成
	public ArrayList<CategoryDTO> getCategoryList() {
		// 変数宣言
		ArrayList<CategoryDTO> list = new ArrayList<CategoryDTO>();

		try (Connection conn = ConnectionManager.getConnection()) {

			// SQL文の作成
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("				*");
			sb.append("FROM");
			sb.append("				CATEGORY");

			PreparedStatement ps = conn.prepareStatement(sb.toString());

			// 実行
			ResultSet rs = ps.executeQuery();

			// 結果をリストに格納
			while (rs.next()) {
				CategoryDTO dto = new CategoryDTO();
				dto.setCategoryId(rs.getInt(1));
				dto.setCategoryName(rs.getString(2));

				list.add(dto);
			}
//			System.out.println(list.get(0).getCategoryName());
			return list;
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		}

		return list;
	}

	// ジャンルリストの作成
	public ArrayList<GenreDTO> getGenreList() {
		ArrayList<GenreDTO> list = new ArrayList<GenreDTO>();

		try (Connection conn = ConnectionManager.getConnection()) {

			// SQL文の作成
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("				*");
			sb.append("FROM");
			sb.append("				GENRE");

			PreparedStatement ps = conn.prepareStatement(sb.toString());

			// 実行
			ResultSet rs = ps.executeQuery();

			// 結果をリストに格納
			while (rs.next()) {
				GenreDTO dto = new GenreDTO();
				dto.setGenreId(rs.getInt(1));
				dto.setGenreName(rs.getString(2));
				dto.setCategoryId(rs.getInt(3));
				list.add(dto);
			}
		} catch (SQLException | NamingException e) {

		}

		return list;
	}

	// 商品検索(カテゴリ、ジャンル未選択、検索キーワード未入力)
	public ArrayList<ItemDTO> searchAllItem() {
		// 変数宣言
		ArrayList<ItemDTO> list = new ArrayList<ItemDTO>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try (Connection conn = ConnectionManager.getConnection()) {
			// SQL文の作成
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("			ITEM_ID,");
			sb.append("			CATEGORY_ID,");
			sb.append("			GENRE_ID,");
			sb.append("			ITEM_NAME,");
			sb.append("			PRICE,");
			sb.append("			ARTIST");
			sb.append("		FROM");
			sb.append("			ITEM");

			ps = conn.prepareStatement(sb.toString());

			// 実行
			rs = ps.executeQuery();

			// 結果の格納
			while (rs.next()) {
				ItemDTO dto = new ItemDTO();
				dto.setItemId(rs.getInt(1));
				dto.setCategoryId(rs.getInt(2));
				dto.setGenreId(rs.getInt(3));
				dto.setItemName(rs.getString(4));
				dto.setPrice(rs.getInt(5));
				dto.setArtist(rs.getString(6));

				list.add(dto);
			}

			return list;

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		}
		return list;
	}

	// 商品検索(ジャンル選択、キーワード入力、カテゴリ未選択)
	public ArrayList<ItemDTO> searchItem(int genreId, String word) {
		// 変数宣言
		ArrayList<ItemDTO> list = new ArrayList<ItemDTO>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try (Connection conn = ConnectionManager.getConnection()) {
			// SQL文の作成
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("			ITEM_ID,");
			sb.append("			CATEGORY_ID,");
			sb.append("			GENRE_ID,");
			sb.append("			ITEM_NAME,");
			sb.append("			PRICE,");
			sb.append("			ARTIST");
			sb.append("		FROM");
			sb.append("			ITEM");
			sb.append("		WHERE");
			sb.append("			GENRE_ID = ?");
			sb.append("			AND");
			sb.append("			ITEM_NAME LIKE ? ");

			ps = conn.prepareStatement(sb.toString());
			ps.setInt(1, genreId);
			word = Normalizer.normalize(word, Normalizer.Form.NFKC);
			ps.setString(2, String.format("%1$s%2$s%1$s", "%", word));

			// 実行
			rs = ps.executeQuery();

			// 結果の格納
			while (rs.next()) {
				ItemDTO dto = new ItemDTO();
				dto.setItemId(rs.getInt(1));
				dto.setCategoryId(rs.getInt(2));
				dto.setGenreId(rs.getInt(3));
				dto.setItemName(rs.getString(4));
				dto.setPrice(rs.getInt(5));
				dto.setArtist(rs.getString(6));

				list.add(dto);
			}

			return list;

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		}

		return list;
	}

	// 商品検索(検索キーワード、カテゴリ、ジャンルが記入選択済み)
	public ArrayList<ItemDTO> searchItem(String word, int categoryId, int genreId) {
		// 変数宣言
		ArrayList<ItemDTO> list = new ArrayList<ItemDTO>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try (Connection conn = ConnectionManager.getConnection()) {
			// SQL文の作成
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("			ITEM_ID,");
			sb.append("			CATEGORY_ID,");
			sb.append("			GENRE_ID,");
			sb.append("			ITEM_NAME,");
			sb.append("			PRICE,");
			sb.append("			ARTIST");
			sb.append("		FROM");
			sb.append("			ITEM");
			sb.append("		WHERE");
			sb.append("			CATEGORY_ID = ?");
			sb.append("			AND");
			sb.append("			GENRE_ID = ?");
			sb.append("			AND");
			sb.append("			ITEM_NAME LIKE ? ");

			ps = conn.prepareStatement(sb.toString());
			word = Normalizer.normalize(word, Normalizer.Form.NFKC);

			ps.setInt(1, categoryId);
			ps.setInt(2, genreId);
			ps.setString(3, String.format("%1$s%2$s%1$s", "%", word));
			// ps.setString(3, "'%" + word + "%'");

			// 実行
			rs = ps.executeQuery();

			// 結果の格納
			while (rs.next()) {
				ItemDTO dto = new ItemDTO();
				dto.setItemId(rs.getInt(1));
				dto.setCategoryId(rs.getInt(2));
				dto.setGenreId(rs.getInt(3));
				dto.setItemName(rs.getString(4));
				dto.setPrice(rs.getInt(5));
				dto.setArtist(rs.getString(6));

				list.add(dto);
			}

			return list;

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		}
		return list;
	}

	// 商品検索(検索キーワード、カテゴリが記入選択済み、ジャンルは未選択)
	public ArrayList<ItemDTO> searchItem(String word, int categoryId) {
		// 変数宣言
		ArrayList<ItemDTO> list = new ArrayList<ItemDTO>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try (Connection conn = ConnectionManager.getConnection()) {
			// SQL文の作成
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("			ITEM_ID,");
			sb.append("			CATEGORY_ID,");
			sb.append("			GENRE_ID,");
			sb.append("			ITEM_NAME,");
			sb.append("			PRICE,");
			sb.append("			ARTIST");
			sb.append("		FROM");
			sb.append("			ITEM");
			sb.append("		WHERE");
			sb.append("			CATEGORY_ID = ?");
			sb.append("			AND");
			sb.append("			ITEM_NAME LIKE ? ");

			ps = conn.prepareStatement(sb.toString());
			ps.setInt(1, categoryId);
			word = Normalizer.normalize(word, Normalizer.Form.NFKC);
			ps.setString(2, String.format("%1$s%2$s%1$s", "%", word));

			// 実行
			rs = ps.executeQuery();

			// 結果の格納
			while (rs.next()) {
				ItemDTO dto = new ItemDTO();
				dto.setItemId(rs.getInt(1));
				dto.setCategoryId(rs.getInt(2));
				dto.setGenreId(rs.getInt(3));
				dto.setItemName(rs.getString(4));
				dto.setPrice(rs.getInt(5));
				dto.setArtist(rs.getString(6));

				list.add(dto);
			}

			return list;

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		}

		return list;
	}

	// 商品検索(ジャンルが選択済み、カテゴリ未選択検索キーワード未入力)
	public ArrayList<ItemDTO> searchItem(int genreId) {
		// 変数宣言
		ArrayList<ItemDTO> list = new ArrayList<ItemDTO>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try (Connection conn = ConnectionManager.getConnection()) {
			// SQL文の作成
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("			ITEM_ID,");
			sb.append("			CATEGORY_ID,");
			sb.append("			GENRE_ID,");
			sb.append("			ITEM_NAME,");
			sb.append("			PRICE,");
			sb.append("			ARTIST");
			sb.append("		FROM");
			sb.append("			ITEM");
			sb.append("		WHERE");
			sb.append("			GENRE_ID = ?");

			ps = conn.prepareStatement(sb.toString());
			ps.setInt(1, genreId);

			// 実行
			rs = ps.executeQuery();

			// 結果の格納
			while (rs.next()) {
				ItemDTO dto = new ItemDTO();
				dto.setItemId(rs.getInt(1));
				dto.setCategoryId(rs.getInt(2));
				dto.setGenreId(rs.getInt(3));
				dto.setItemName(rs.getString(4));
				dto.setPrice(rs.getInt(5));
				dto.setArtist(rs.getString(6));

				list.add(dto);
			}

			return list;

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		}

		return list;
	}

	// 商品検索(カテゴリが選択済み、カテゴリ未選択検索キーワード未入力)
	public ArrayList<ItemDTO> searchItem(String categoryId) {
		// 変数宣言
		ArrayList<ItemDTO> list = new ArrayList<ItemDTO>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try (Connection conn = ConnectionManager.getConnection()) {
			// SQL文の作成
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("			ITEM_ID,");
			sb.append("			CATEGORY_ID,");
			sb.append("			GENRE_ID,");
			sb.append("			ITEM_NAME,");
			sb.append("			PRICE,");
			sb.append("			ARTIST");
			sb.append("		FROM");
			sb.append("			ITEM");
			sb.append("		WHERE");
			sb.append("			CATEGORY_ID = ?");

			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, categoryId);

			// 実行
			rs = ps.executeQuery();

			// 結果の格納
			while (rs.next()) {
				ItemDTO dto = new ItemDTO();
				dto.setItemId(rs.getInt(1));
				dto.setCategoryId(rs.getInt(2));
				dto.setGenreId(rs.getInt(3));
				dto.setItemName(rs.getString(4));
				dto.setPrice(rs.getInt(5));
				dto.setArtist(rs.getString(6));

				list.add(dto);
			}

			return list;

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		}

		return list;
	}

	// 商品検索(カテゴリ、ジャンルが選択、検索キーワード未入力)
	public ArrayList<ItemDTO> searchItem(int categoryId, int genreId) {
		// 変数宣言
		ArrayList<ItemDTO> list = new ArrayList<ItemDTO>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try (Connection conn = ConnectionManager.getConnection()) {
			// SQL文の作成
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("			ITEM_ID,");
			sb.append("			CATEGORY_ID,");
			sb.append("			GENRE_ID,");
			sb.append("			ITEM_NAME,");
			sb.append("			PRICE,");
			sb.append("			ARTIST");
			sb.append("		FROM");
			sb.append("			ITEM");
			sb.append("		WHERE");
			sb.append("			CATEGORY_ID = ?");
			sb.append("			AND");
			sb.append("			GENRE_ID = ?");

			ps = conn.prepareStatement(sb.toString());
			ps.setInt(1, categoryId);
			ps.setInt(2, genreId);

			// 実行
			rs = ps.executeQuery();

			// 結果の格納
			while (rs.next()) {
				ItemDTO dto = new ItemDTO();
				dto.setItemId(rs.getInt(1));
				dto.setCategoryId(rs.getInt(2));
				dto.setGenreId(rs.getInt(3));
				dto.setItemName(rs.getString(4));
				dto.setPrice(rs.getInt(5));
				dto.setArtist(rs.getString(6));

				list.add(dto);
			}

			return list;

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		}
		return list;
	}

	// 商品検索(キーワードだけで検索)
	public ArrayList<ItemDTO> searchKeyword(String word) {
		// 変数宣言
		ArrayList<ItemDTO> list = new ArrayList<ItemDTO>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try (Connection conn = ConnectionManager.getConnection()) {
			// SQL文の作成
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("			ITEM_ID,");
			sb.append("			CATEGORY_ID,");
			sb.append("			GENRE_ID,");
			sb.append("			ITEM_NAME,");
			sb.append("			PRICE,");
			sb.append("			ARTIST");
			sb.append("		FROM");
			sb.append("			ITEM");
			sb.append("		WHERE");
			sb.append("			ITEM_NAME LIKE ?");

			ps = conn.prepareStatement(sb.toString());
			word = Normalizer.normalize(word, Normalizer.Form.NFKC);
			ps.setString(1, String.format("%1$s%2$s%1$s", "%", word));

			// 実行
			rs = ps.executeQuery();

			// 結果の格納
			while (rs.next()) {
				ItemDTO dto = new ItemDTO();
				dto.setItemId(rs.getInt(1));
				dto.setCategoryId(rs.getInt(2));
				dto.setGenreId(rs.getInt(3));
				dto.setItemName(rs.getString(4));
				dto.setPrice(rs.getInt(5));
				dto.setArtist(rs.getString(6));

				list.add(dto);
			}

			return list;

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		}
		return list;
	}

}
