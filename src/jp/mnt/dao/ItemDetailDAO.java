package jp.mnt.dao;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.imageio.ImageIO;
import javax.naming.NamingException;

import jp.mnt.dto.ItemDTO;

public class ItemDetailDAO{

	//商品一件取得
	public ItemDTO selectItem(int itemId){
		//変数宣言
		ItemDTO dto = new ItemDTO();
		try (Connection conn = ConnectionManager.getConnection()) {
			// SQL文の作成
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        ITEM_ID,");
			sb.append("        ITEM_NAME,");
			sb.append("        CATEGORY_ID,");
			sb.append("        GENRE_ID,");
			sb.append("        NEW_AND_OLD_ID,");
			sb.append("        ARTIST,");
			sb.append("        PRICE,");
			sb.append("        REMARKS");
			sb.append("    FROM");
			sb.append("        ITEM");
			sb.append("    WHERE");
			sb.append("        ITEM_ID = ?");

			PreparedStatement ps = conn.prepareStatement(sb.toString());
			ps.setInt(1, itemId);

			// 実行
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				// 結果を変数に格納する
				dto.setItemId(rs.getInt(1));
				dto.setItemName(rs.getString(2));
				dto.setCategoryId(rs.getInt(3));
				dto.setGenreId(rs.getInt(4));
				dto.setNewAndOldId(rs.getString(5));
				dto.setArtist(rs.getString(6));
				dto.setPrice(rs.getInt(7));
				dto.setRemarks(rs.getString(8));
			}
			return dto;

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		}
		return dto;
	}

	//指定されたIDの画像を復元
	public BufferedImage selectImageById(int itemId){
		try (Connection conn = ConnectionManager.getConnection()){

			// SQL文を作成する
			StringBuffer sbSQL = new StringBuffer();
			sbSQL.append("   SELECT");
			sbSQL.append("          IMAGE");
			sbSQL.append("     FROM");
			sbSQL.append("          ITEM");
			sbSQL.append("    WHERE");
			sbSQL.append("          ITEM_ID = ?");

			//実行する
			PreparedStatement ps = conn.prepareStatement(sbSQL.toString());
			ps.setInt(1, itemId);
			ResultSet rs = ps.executeQuery();

			// 結果から画像データを取得し、返す。
			if (rs.next()) {
				InputStream is = rs.getBinaryStream("IMAGE");
				BufferedInputStream bis = new BufferedInputStream(is);
				return ImageIO.read(bis);
			}

		} catch (IOException | SQLException | NamingException e) {
			e.printStackTrace();
		}

		return null;
	}

	//商品IDからカテゴリ名を取得
	public String getCategoryName(int categoryId){
		String categoryName = null;

		try (Connection conn = ConnectionManager.getConnection()){
			// SQL文を作成する
			StringBuffer sbSQL = new StringBuffer();
			sbSQL.append("   SELECT");
			sbSQL.append("          CATEGORY_NAME");
			sbSQL.append("     FROM");
			sbSQL.append("          CATEGORY");
			sbSQL.append("    WHERE");
			sbSQL.append("          CATEGORY_ID = ?");

			PreparedStatement ps = conn.prepareStatement(sbSQL.toString());
			ps.setInt(1, categoryId);

			//実行
			ResultSet rs = ps.executeQuery();

			//結果を変数に格納する
			if(rs.next()){
				categoryName = rs.getString(1);
			}
		}
		catch (SQLException | NamingException e) {
			e.printStackTrace();
		}
		return categoryName;
	}

	//商品IDからジャンル名を取得
	public String getGenreName(int genreId) {
		String genreName = null;

		try (Connection conn = ConnectionManager.getConnection()) {
			// SQL文を作成する
			StringBuffer sbSQL = new StringBuffer();
			sbSQL.append("   SELECT");
			sbSQL.append("          GENRE_NAME");
			sbSQL.append("     FROM");
			sbSQL.append("          GENRE");
			sbSQL.append("    WHERE");
			sbSQL.append("          GENRE_ID = ?");

			PreparedStatement ps = conn.prepareStatement(sbSQL.toString());
			ps.setInt(1, genreId);

			// 実行
			ResultSet rs = ps.executeQuery();

			// 結果を変数に格納する
			if (rs.next()) {
				genreName = rs.getString(1);
			}
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		}
		return genreName;
	}

	//商品IDから新旧区分名を取得
	public String getNewAndOldName(String newAndOldId) {
		String newAndOldName = null;

		try (Connection conn = ConnectionManager.getConnection()) {
			// SQL文を作成する
			StringBuffer sbSQL = new StringBuffer();
			sbSQL.append("   SELECT");
			sbSQL.append("          NEW_AND_OLD_NAME");
			sbSQL.append("     FROM");
			sbSQL.append("          NEW_AND_OLD");
			sbSQL.append("    WHERE");
			sbSQL.append("          NEW_AND_OLD_ID = ?");

			PreparedStatement ps = conn.prepareStatement(sbSQL.toString());

			ps.setString(1, newAndOldId);

			// 実行
			ResultSet rs = ps.executeQuery();

			// 結果を変数に格納する
			if (rs.next()) {
				newAndOldName = rs.getString(1);
			}

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		}
		return newAndOldName;
	}
}
