package jp.mnt.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import jp.mnt.dto.UserInsertDTO;

public class UserInsertDAO {
	Connection conn = null;

	public UserInsertDAO(Connection conn) {
		super();
		this.conn = conn;
	}

	public int userInsert(UserInsertDTO dto) throws SQLException{


		//SQLの作成
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT INTO");
		sb.append("	USER");
		sb.append("	(USER_ID, PASSWORD, LAST_NAME, FIRST_NAME, POSTAL_CODE, ADDRESS1, ADDRESS2, TEL, BIRTHDAY, CARD_NUMBER, CARD_NAME, CARD_EXPIRATION_YEAR, CARD_EXPIRATION_MONTH, DELETE_FLG)");
		sb.append("	VALUES");
		sb.append("	( ?, PASSWORD(?), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  0 )");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			//検索キーのセット
			ps.setString(1, dto.getUserId());
			ps.setString(2, dto.getPassword());
			ps.setString(3, dto.getLastName());
			ps.setString(4, dto.getFirstName());
			ps.setString(5, dto.getPostalCode());
			ps.setString(6, dto.getAddress1());
			ps.setString(7, dto.getAddress2());
			ps.setString(8, dto.getTel());
			ps.setDate(9, Date.valueOf(dto.getBirthday()));
			ps.setString(10, dto.getCardNumber());
			ps.setString(11, dto.getCardName());
			ps.setString(12, dto.getCardExpirationYear());
			ps.setString(13, dto.getCardExpirationMonth());


			return ps.executeUpdate();



		}

	}
}
