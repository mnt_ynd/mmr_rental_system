package jp.mnt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;

import jp.mnt.dto.ItemDTO;

public class HomeDAO {
	Connection conn;

	public HomeDAO(Connection conn) {
		super();
		this.conn = conn;
	}

	// おすすめ商品を取得してくる
	public ArrayList<ItemDTO> recommendList() {
		// 変数宣言
		ArrayList<ItemDTO> list = new ArrayList<ItemDTO>();

		try (Connection conn = ConnectionManager.getConnection()) {

			// SQL文の作成
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        ITEM_ID");
			sb.append("        ,item.CATEGORY_ID");
			sb.append("        ,item.NEW_AND_OLD_ID");
			sb.append("        ,ITEM_NAME");
			sb.append("        ,PRICE");
			sb.append("        ,CATEGORY_NAME");
			sb.append("        ,NEW_AND_OLD_NAME");
			sb.append("    FROM");
			sb.append("        item");
			sb.append("            LEFT JOIN CATEGORY");
			sb.append("                ON item.CATEGORY_ID = category.CATEGORY_ID");
			sb.append("            LEFT JOIN new_and_old");
			sb.append("                ON item.NEW_AND_OLD_ID = new_and_old.NEW_AND_OLD_ID");
			sb.append("    WHERE");
			sb.append("        RECOMMENDED_FLG = ?");
			sb.append("    ORDER BY");
			sb.append("        RAND() LIMIT 10;");

			PreparedStatement ps = conn.prepareStatement(sb.toString());
			ps.setInt(1, 1);

			// 実行
			ResultSet rs = ps.executeQuery();

			// 結果をリストに格納
			while (rs.next()) {
				ItemDTO dto = new ItemDTO();
				dto.setItemId(rs.getInt("ITEM_ID"));
				dto.setCategoryId(rs.getInt("CATEGORY_ID"));
				dto.setNewAndOldId(rs.getString("NEW_AND_OLD_ID"));
				dto.setItemName(rs.getString("ITEM_NAME"));
				dto.setPrice(rs.getInt("PRICE"));
				dto.setCategoryName(rs.getString("CATEGORY_NAME"));
				dto.setNewAndOldName(rs.getString("NEW_AND_OLD_NAME"));
				list.add(dto);
			}
			return list;
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		}
		return list;
	}

}
