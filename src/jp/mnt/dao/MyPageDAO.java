package jp.mnt.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.NamingException;

import jp.mnt.dto.UserDTO;

public class MyPageDAO {

	public UserDTO getData(int index){
		try (Connection conn = ConnectionManager.getConnection()) {
			//変数宣言
			PreparedStatement ps = null;
			ResultSet rs = null;
			UserDTO dto = new UserDTO();

			//SQL作成
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        USER_ID,");
			sb.append("        PASSWORD,");
			sb.append("        POSTAL_CODE,");
			sb.append("        ADDRESS1,");
			sb.append("        ADDRESS2,");
			sb.append("        TEL,");
			sb.append("        BIRTHDAY,");
			sb.append("        CARD_NUMBER,");
			sb.append("        CARD_NAME,");
			sb.append("        CARD_EXPIRATION_YEAR,");
			sb.append("        CARD_EXPIRATION_MONTH");
			sb.append("    FROM");
			sb.append("        USER");
			sb.append("    WHERE");
			sb.append("        USER_INDEX = ?");

			ps = conn.prepareStatement(sb.toString());
			ps.setInt(1, index);

			//実行
			rs = ps.executeQuery();

			//結果を取得
			if(rs.next()){
				dto.setUserId(rs.getString(1));
				dto.setPassword(rs.getString(2));
				dto.setPostalCode(rs.getString(3));
				dto.setAddress1(rs.getString(4));
				dto.setAddress2(rs.getString(5));
				dto.setTel(rs.getString(6));
				dto.setBirthday(rs.getDate(7));
				dto.setCardNumber(rs.getString(8));
				dto.setCardName(rs.getString(9));
				dto.setCardExpirationYear(rs.getString(10));
				dto.setCardExpirationMonth(rs.getString(11));
			}

			return dto;
		}
		catch(SQLException | NamingException e) {
			e.printStackTrace();

		}
		return null;
	}

	//引数のindexの顧客データが存在するかチェックする。
	public boolean checkIndex(int index){
		//変数宣言
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean returnFlag = false;

		try (Connection conn = ConnectionManager.getConnection()) {
			//SQL文の作成
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        COUNT(*)");
			sb.append("    FROM");
			sb.append("        USER");
			sb.append("    WHERE");
			sb.append("        USER_INDEX = ?");

			ps = conn.prepareStatement(sb.toString());
			ps.setInt(1, index);

			//実行
			rs = ps.executeQuery();

			//結果を取得
			if(rs.next()){
				if(rs.getInt(1) == 1){
					returnFlag = true;
				}else{
					returnFlag = true;
				}
			}
		}
		catch(SQLException | NamingException e) {
			e.printStackTrace();
		}

		return returnFlag;
	}
}
