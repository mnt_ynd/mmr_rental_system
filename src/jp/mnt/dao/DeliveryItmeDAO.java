package jp.mnt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.mnt.dto.DeliveryTimeDTO;

public class DeliveryItmeDAO {
	Connection conn = null;

	public DeliveryItmeDAO(Connection conn) {
		super();
		this.conn = conn;
	}

	public ArrayList<DeliveryTimeDTO> selectAll() throws SQLException {

		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        DELIVERY_TIME_ID");
		sb.append("        ,DELIVERY_TIME_NAME");
		sb.append("    FROM");
		sb.append("      DELIVERY_TIME;");
		
		// リストの作成
		ArrayList<DeliveryTimeDTO> list = new ArrayList<DeliveryTimeDTO>();
		// SQL文実行
		PreparedStatement ps = conn.prepareStatement(sb.toString());
		ResultSet rs = ps.executeQuery();

		// SQLの結果を取得し、リストに詰める
		while (rs.next()) {
			DeliveryTimeDTO empRowData = new DeliveryTimeDTO();
			empRowData.setDeliveryTimeId(rs.getString("DELIVERY_TIME_ID"));
			empRowData.setDeliveryTimeName(rs.getString("DELIVERY_TIME_NAME"));
			list.add(empRowData);
		}

		return list;

	}
}
