package jp.mnt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAO {
	Connection conn = null;

	public UserDAO(Connection conn) {
		super();
		this.conn = conn;
	}

	//ログインするとき、入力されたIDとパスワードがDB内に存在するかチェックする。
	public boolean selectByIdAndPassword(String id, String password)
			throws SQLException {

		//SQL文作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUNT(*)");
		sb.append("    FROM");
		sb.append("        USER");
		sb.append("    WHERE");
		sb.append("        USER_ID = ?");
		sb.append("        AND PASSWORD = PASSWORD(?)");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			//検索キーのセット
			ps.setString(1, id);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();

			//結果の格納
			if (rs.next()) {
				return rs.getInt(1) == 1;
			}

			return false;

		} catch (SQLException e) {
			throw e;
		}
	}

	//顧客の姓名を取得する。
	public String[] getName(String id,String password)throws SQLException {
		String returnStr[] = null;

		//SQLの作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        LAST_NAME,");
		sb.append("        FIRST_NAME");
		sb.append("    FROM");
		sb.append("        USER");
		sb.append("    WHERE");
		sb.append("        USER_ID = ?");
		sb.append("        AND PASSWORD = PASSWORD(?)");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			//検索キーのセット
			ps.setString(1, id);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();

			//結果の格納
			if (rs.next()) {
				returnStr = new String[2];
				returnStr[0] = rs.getString(1);		//LAST_NAME
				returnStr[1] = rs.getString(2);		//FIRST_NAME
//				System.out.println(returnStr[0] + returnStr[1]);
			}


		} catch (SQLException e) {
			throw e;
		}

		return returnStr;
	}

	//顧客のDBのインデックスを取得
	public int getIndex(String id,String password) throws SQLException{
		int returnNum = -1;

		//SQLの作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        USER_INDEX");
		sb.append("    FROM");
		sb.append("        USER");
		sb.append("    WHERE");
		sb.append("        USER_ID = ?");
		sb.append("        AND PASSWORD = PASSWORD(?)");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			//検索キーのセット
			ps.setString(1, id);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();

			//結果の格納
			if (rs.next()) {
				returnNum = rs.getInt(1);
			}

		} catch (SQLException e) {
			throw e;
		}
		return returnNum;
	}
}
