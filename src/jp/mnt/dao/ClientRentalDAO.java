package jp.mnt.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.mnt.dto.ItemDTO;
import jp.mnt.dto.RentalDTO;

public class ClientRentalDAO {
	Connection conn;

	public ClientRentalDAO(Connection conn) {
		super();
		this.conn = conn;
	}

	/**
	 * クーポンを使用する処理を行い、レンタル処理を行ったあとに、RENTAL_NUMBERを返す
	 *
	 * @param dto
	 * @return
	 * @throws SQLException
	 */
	public int insertAll(RentalDTO dto) throws SQLException {
		// SQL文生成
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        rental(");
		sb.append("            RENTAL_NUMBER");
		sb.append("            ,USER_INDEX");
		sb.append("            ,ORDER_DATETIME");
		sb.append("            ,DELIVERY_DATE");
		sb.append("            ,DELIVERY_TIME_ID");
		sb.append("            ,STATUS_ID");
		sb.append("            ,COUPON_DISTRIBUTION_INDEX");
		sb.append("            ,DISCOUNT");
		sb.append("        )");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            null");
		sb.append("            ,?");
		sb.append("            ,now()");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,'1'");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("        );");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString());) {
			// SQL文実行

			ps.setInt(1, Integer.parseInt(dto.getUserIndex()));
			ps.setDate(2, Date.valueOf(dto.getDeliveryDate()));
			ps.setString(3, dto.getDeliveryTimeId());
			ps.setInt(4, Integer.parseInt(dto.getCouponIndex()));
			ps.setInt(5, Integer.parseInt(dto.getDiscount()));

			// レンタルテーブルにレンタル情報をインサートできたら、その時に発行されたRENTAL_NUMBERを取得する
			if (ps.executeUpdate() == 1) {
				// RENTAL_NUMBER取得用SQL
				StringBuffer sb2 = new StringBuffer();
				sb2.append("SELECT");
				sb2.append("        MAX(RENTAL_NUMBER) AS RENTAL_NUMBER");
				sb2.append("    FROM");
				sb2.append("        rental");
				sb2.append("    WHERE");
				sb2.append("        USER_INDEX = ?");

				try (PreparedStatement ps2 = conn.prepareStatement(sb2.toString())) {
					ps2.setInt(1, Integer.parseInt(dto.getUserIndex()));

					// 実行
					ResultSet rs = ps2.executeQuery();

					// 結果をリストに格納
					while (rs.next()) {
						return rs.getInt("RENTAL_NUMBER");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			// なしかしらエラー
			return 0;
		}
	}

	/**
	 * クーポンを使用しないでレンタル処理を行い、RENTAL_NUMBERを返す。
	 *
	 * @param dto
	 * @return
	 * @throws SQLException
	 */
	public int insert(RentalDTO dto) throws SQLException {
		// SQL文
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        rental(");
		sb.append("            RENTAL_NUMBER");
		sb.append("            ,USER_INDEX");
		sb.append("            ,ORDER_DATETIME");
		sb.append("            ,DELIVERY_DATE");
		sb.append("            ,DELIVERY_TIME_ID");
		sb.append("            ,STATUS_ID");
		sb.append("        )");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            null");
		sb.append("            ,?");
		sb.append("            ,now()");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,'1'");
		sb.append("        );");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString());) {
			// SQL文実行

			ps.setInt(1, Integer.parseInt(dto.getUserIndex()));
			ps.setDate(2, Date.valueOf(dto.getDeliveryDate()));
			ps.setString(3, dto.getDeliveryTimeId());

			// レンタル情報の登録完了
			if (ps.executeUpdate() == 1) {
				// RENTAL_NUMBER取得用SQL
				StringBuffer sb2 = new StringBuffer();
				sb2.append("SELECT");
				sb2.append("        MAX(RENTAL_NUMBER) AS RENTAL_NUMBER");
				sb2.append("    FROM");
				sb2.append("        rental");
				sb2.append("    WHERE");
				sb2.append("        USER_INDEX = ?");

				try (PreparedStatement ps2 = conn.prepareStatement(sb2.toString())) {
					ps2.setInt(1, Integer.parseInt(dto.getUserIndex()));

					// 実行
					ResultSet rs = ps2.executeQuery();

					// 結果をリストに格納
					while (rs.next()) {
						return rs.getInt("RENTAL_NUMBER");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return 0;
		}
	}

	/**
	 * レンタル情報を元に商品の紐付けを行う
	 *
	 * @param itemList
	 * @param rentalNumber
	 * @return
	 */
	public int rentalItem(ArrayList<ItemDTO> itemList, int rentalNumber) {
		// SQL文作成
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        rental_detail(");
		sb.append("            RENTAL_DETAIL_NUMBER");
		sb.append("            ,RENTAL_NUMBER");
		sb.append("            ,ITEM_ID");
		sb.append("            ,PRICE");
		sb.append("        )");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            null");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,(");
		sb.append("                    SELECT");
		sb.append("                            PRICE");
		sb.append("                        FROM");
		sb.append("                            item");
		sb.append("                        WHERE");
		sb.append("                            ITEM_ID = ?");
		sb.append("            )");
		sb.append("        );");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			int count = 0;
			// 商品の数だけ繰り返す
			for (ItemDTO item : itemList) {
				ps.setInt(1, rentalNumber);
				ps.setInt(2, item.getItemId());
				ps.setInt(3, item.getItemId());
				count += ps.executeUpdate();
			}
			return count;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
}
