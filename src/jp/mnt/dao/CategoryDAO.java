package jp.mnt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CategoryDAO {
	Connection conn = null;

	public CategoryDAO(Connection conn) {
		super();
		this.conn = conn;
	}

	public String selectCouponName(int couponId) throws SQLException {

		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUPON_NAME");
		sb.append("    FROM");
		sb.append("     COUPON");
		sb.append("        WHERE");
		sb.append("            COUPON_ID = ?");
		
		
		// SQL文実行
		PreparedStatement ps = conn.prepareStatement(sb.toString());
		
		ps.setInt(1, couponId);
		
		ResultSet rs = ps.executeQuery();

		// SQLの結果を取得し、リストに詰める
		while (rs.next()) {
			
			return rs.getString("COUPON_NAME");
			
		}

		return null;

	}
}
