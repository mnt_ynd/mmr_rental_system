package jp.mnt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.mnt.dto.CouponDTO;

public class CouponDistributionSelectDAO {
	
	Connection conn = null;

	public CouponDistributionSelectDAO(Connection conn) {
		super();
		this.conn = conn;
	}
	
	public int haveCoupon(int index) throws SQLException {

		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUPON_ID");
		sb.append("    FROM");
		sb.append("      COUPON_DISTRIBUTION");
		sb.append("        WHERE");
		sb.append("            USER_INDEX = ?");
		sb.append("        AND DATE_ADD(LOTTERY_DATE, INTERVAL 1 DAY) >= NOW()");
		sb.append("        AND USE_FLG = false");
		
		
		// SQL文実行
		PreparedStatement ps = conn.prepareStatement(sb.toString());
		
		ps.setInt(1, index);
		
		ResultSet rs = ps.executeQuery();

		// SQLの結果を取得し、値を返す
		while (rs.next()) {
			
			return rs.getInt("COUPON_ID");
			
		}

		return 0;

	}
	
	public int couponIndex(int id) throws SQLException {

		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("	COUPON_DISTRIBUTION_INDEX");
		sb.append("	FROM");
		sb.append("	COUPON_DISTRIBUTION");
		sb.append(" WHERE");
		sb.append("	USER_INDEX = ?");
		sb.append(" AND DATE_ADD(LOTTERY_DATE, INTERVAL 1 DAY) >= NOW()");
		sb.append(" AND USE_FLG = false");
		
		
		// SQL文実行
		PreparedStatement ps = conn.prepareStatement(sb.toString());
		
		ps.setInt(1, id);
		
		ResultSet rs = ps.executeQuery();

		// SQLの結果を取得し、値を返す
		while (rs.next()) {
			
			return rs.getInt("COUPON_DISTRIBUTION_INDEX");
			
		}

		return 0;

	}
	
	public CouponDTO selectCouponName(int couponId) throws SQLException {

		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUPON_NAME");
		sb.append("        ,DISCOUNT");
		sb.append("    FROM");
		sb.append("     COUPON");
		sb.append("        WHERE");
		sb.append("            COUPON_ID = ?");
		
		
		
		// SQL文実行
		PreparedStatement ps = conn.prepareStatement(sb.toString());
		
		ps.setInt(1, couponId);
		
		ResultSet rs = ps.executeQuery();
		CouponDTO dto = new CouponDTO();
		
		// SQLの結果を取得し、リストに詰める
		while (rs.next()) {
			
			dto.setCouponName(rs.getString("COUPON_NAME"));
			dto.setDiscount(rs.getInt("DISCOUNT"));
			
			return dto;
		}

		return null;

	}
	
	public int useCoupon(int couponId) throws SQLException {

		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE");
		sb.append("        COUPON_DISTRIBUTION");
		sb.append("    SET");
		sb.append("        USE_FLG = true");
		sb.append("    WHERE");
		sb.append("        COUPON_ID = ?");
		sb.append("        AND DATE_ADD(LOTTERY_DATE, INTERVAL 1 DAY) >= NOW()");
		sb.append("        AND USE_FLG = false");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			
			ps.setInt(1, couponId);

			return ps.executeUpdate();
			
			

		} catch (SQLException e) {
			throw e;
			
		} catch (NullPointerException e) {
			throw e;
		}
	}
	
	public int insertCoupon(int userId, int couponid) throws SQLException {

		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO");
		sb.append("	COUPON_DISTRIBUTION");
		sb.append("	(COUPON_DISTRIBUTION_INDEX, USER_INDEX, COUPON_ID, LOTTERY_DATE, USE_FLG)");
		sb.append("	VALUES");
		sb.append("	(null, ?, ?, NOW(), false)");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			
			ps.setInt(1, userId);
			ps.setInt(2, couponid);

			return ps.executeUpdate();
			
			

		} catch (SQLException e) {
			throw e;
			
		} catch (NullPointerException e) {
			throw e;
		}
	}
	
	
		

		

	
}
