package jp.mnt.check;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class Check {

	public static boolean nameCheck(String str) {

		String pattern2 = "^[^ /:-@\\[-\\`\\{-\\~*]+$";
		//String pattern = "^[* /:-@\\[-\\`\\{-\\~]+$";
		
		
		return str.length() <= 20 && str.matches(pattern2) ;
				//|| !str.matches(pattern);
	}
	
	

	public static boolean addrCheck(String str) {

		
		String pattern = "^[^ /:-@\\[-\\`\\{-\\~]+$";

		return str.length() <= 50 && str.matches(pattern);
	}

	public static boolean mailCheck(String str) {

		String matchstr = "([a-zA-Z0-9][a-zA-Z0-9_.+\\-]*)@(([a-zA-Z0-9][a-zA-Z0-9_\\-]+\\.)+[a-zA-Z]{2,6})";

		return str.length() <= 80 && str.matches(matchstr);
	}

	public static boolean cardIntCheck(String str) {

		String matchstr = "^[0-9]+$";

		return str.length() <= 20 && str.matches(matchstr);
	}

	public static boolean postalCheck(String str) {

		String matchstr = "^[0-9]+$";

		return str.length() <= 7 && str.matches(matchstr);
	}

	public static boolean telCheck(String str) {

		String matchstr = "^[0-9]+$";

		return str.length() <= 11 && str.matches(matchstr);
	}

	public static boolean cardNameCheck(String str) {

//		String pattern = "^[^,<>\\\\?/_\\!\"#\\$%&'\\(\\)\\-=\\^~\\|;:\\[\\]\\{\\}\\*+]+$";
//		String pattern2 = "^[ ]";
//		String pattern3 = "^[\\.]";
//		String str1 = "ASAFAGA";
		String pattern5 = "^[a-zA-Z0-9]+[\\.\\s][a-zA-Z0-9]+$";
		
		return str.length() <= 255 &&  str.matches(pattern5);
	}

	public static boolean passCheck(String str, String str2) {

		
			String pattern = "^[a-zA-Z0-9]+$";
			
			return str.equals(str2) && str.length() <= 41 && str.matches(pattern) ;
	}

	public static boolean checkDate(String strDate) throws ParseException {

		Calendar cal = Calendar.getInstance();

		Date today = cal.getTime();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date formatDate = sdf.parse(strDate);

		return today.after(formatDate);

	}

	public static boolean checkDate2(String year, String month) throws ParseException {

		String str = year + "-" + month;

		Calendar cal = Calendar.getInstance();

		Date today = cal.getTime();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
		Date formatDate = sdf.parse(str);

		return today.before(formatDate);

	}

}
