package jp.mnt.user;

import java.io.IOException;
import java.sql.Connection;
import java.text.ParseException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.check.Check;
import jp.mnt.dao.ConnectionManager;
import jp.mnt.dao.UserInsertDAO;
import jp.mnt.dto.UserInsertDTO;

/**
 * Servlet implementation class InsertUserServlet
 */
@WebServlet("/insertUser")
public class InsertUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッションの取得
		HttpSession session = request.getSession(false);
		request.setCharacterEncoding("UTF-8");

		UserInsertDTO userDto = new UserInsertDTO();
		String error = "エラー";
		boolean errorFlg = true;
		String lastName = (String) request.getParameter("lastName");
//		System.out.println(lastName);
		userDto.setLastName(lastName);
		if(!Check.nameCheck(lastName)){
			request.setAttribute("lastName", "文字数が超えているか登録できない文字が入力されています");
			errorFlg = false;
		}
		String firstName = (String) request.getParameter("firsName");
//		System.out.println(firstName);
		userDto.setFirstName(firstName);
		if(!Check.nameCheck(firstName)){
			request.setAttribute("firstName", "文字数が超えているか登録できない文字が入力されています");
			errorFlg = false;
		}
		String email = (String) request.getParameter("email");
//		System.out.println(email);
		userDto.setUserId(email);
		if(!Check.mailCheck(email)){
			request.setAttribute("email", "文字数が超えているか登録できない文字が入力されています");
			errorFlg = false;
		}
		String password = (String) request.getParameter("password");
//		System.out.println(password);
		userDto.setPassword(password);
		String password2 = (String) request.getParameter("password2");
//		System.out.println(password2);
		if(!Check.passCheck(password, password2)){
			request.setAttribute("password", "文字数が超えているか確認と違う文字列か半角英数字以外が入力されています");
			errorFlg = false;
		}
		String zip11 = (String) request.getParameter("zip11");
	//	System.out.println(zip11);
		userDto.setPostalCode(zip11);
		if(!Check.postalCheck(zip11)){
			request.setAttribute("zip11", "文字数が超えているか数字以外が入力されています");
			errorFlg = false;
		}
		String address = (String) request.getParameter("addr11");
//		System.out.println(address);
		userDto.setAddress1(address);
		if(!Check.addrCheck(address)){
			request.setAttribute("address", "文字数が超えているか登録できない文字が入力されています");
			errorFlg = false;
		}
		String address2 = (String) request.getParameter("address2");
	//	System.out.println(address2);
		userDto.setAddress2(address2);
		if(!Check.addrCheck(address2)){
			request.setAttribute("address2", "文字数が超えているか登録できない文字が入力されています");
			errorFlg = false;
		}
		String tel = (String) request.getParameter("tel");
//		System.out.println(tel);
		userDto.setTel(tel);
		if(!Check.telCheck(tel)){
			request.setAttribute("tel", "文字数が超えているか数字以外が入力されています");
			errorFlg = false;
		}
		String birthday = (String) request.getParameter("birthday");
//		System.out.println(birthday);
		userDto.setBirthday(birthday);
		try {
			if(!Check.checkDate(birthday)){
				request.setAttribute("birthday", "誕生日が不適切です");
				errorFlg = false;
			}
		} catch (ParseException e1) {

			e1.printStackTrace();
		}
		String creditCard = (String) request.getParameter("creditCard");
//		System.out.println(creditCard);
		userDto.setCardNumber(creditCard);
		if(!Check.cardIntCheck(creditCard)){
			request.setAttribute("creditCard", "文字数が超えているか数字以外が入力されています");
			errorFlg = false;
		}
		String name = (String) request.getParameter("name");
//		System.out.println(name);
		userDto.setCardName(name);
		if(!Check.cardNameCheck(name)){
			request.setAttribute("name", "文字数が超えているか登録できない文字が入力されています");
			errorFlg = false;
		}
		String month = (String) request.getParameter("month");
//		System.out.println(month);
		userDto.setCardExpirationMonth(month);
		String year = (String) request.getParameter("year");
//		System.out.println(year);
		userDto.setCardExpirationYear(year);
		try {
			if(!Check.checkDate2(year, month)){
				request.setAttribute("expiration", "有効期限が不適切です");
				errorFlg = false;
			}
		} catch (ParseException e1) {

			e1.printStackTrace();
		}

		if(!errorFlg){
			request.setAttribute("userDto", userDto);
			request.getRequestDispatcher("/WEB-INF/jsp/addUserView.jsp").forward(request, response);
			return ;
		}

		try (Connection conn = ConnectionManager.getConnection()) {
			conn.setAutoCommit(false);
			UserInsertDAO dao = new UserInsertDAO(conn);

			int userIn = dao.userInsert(userDto);

			if (userIn == 1) {
				conn.commit();
//				System.out.println("commit");
			} else {
				conn.rollback();
//				System.out.println("rollback");
			}

			request.getRequestDispatcher("home").forward(request, response);


		}catch (Exception e) {
			e.printStackTrace();

		}
	}


}
