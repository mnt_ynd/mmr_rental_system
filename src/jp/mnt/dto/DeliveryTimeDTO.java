package jp.mnt.dto;

//お届け日時テーブル
public class DeliveryTimeDTO {
	private String deliveryTimeId;
	
	private String deliveryTimeName;

	public String getDeliveryTimeId() {
		return deliveryTimeId;
	}

	public void setDeliveryTimeId(String deliveryTimeId) {
		this.deliveryTimeId = deliveryTimeId;
	}

	public String getDeliveryTimeName() {
		return deliveryTimeName;
	}

	public void setDeliveryTimeName(String deliveryTimeName) {
		this.deliveryTimeName = deliveryTimeName;
	}
	
	
	
}
