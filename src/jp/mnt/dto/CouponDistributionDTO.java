package jp.mnt.dto;

public class CouponDistributionDTO {
	//クラス変数
	private int couponDistributionIndex;
	private int couponId;
	private int userIndex;

	//setterとgetter
	public int getCouponDistributionIndex() {
		return couponDistributionIndex;
	}
	public void setCouponDistributionIndex(int couponDistributionIndex) {
		this.couponDistributionIndex = couponDistributionIndex;
	}
	public int getCouponId() {
		return couponId;
	}
	public void setCouponId(int couponId) {
		this.couponId = couponId;
	}
	public int getUserIndex() {
		return userIndex;
	}
	public void setUserIndex(int userIndex) {
		this.userIndex = userIndex;
	}
}
