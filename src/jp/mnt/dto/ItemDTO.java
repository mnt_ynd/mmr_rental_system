package jp.mnt.dto;
import java.sql.Timestamp;

public class ItemDTO {
	//クラス変数
	private int itemId;
	private String itemName;
	private int categoryId;
	private int genreId;
	private int recommendedFlg;
	private String newAndOldId;
//	private BufferedImage image;
	private String artist;
	private int price;
	private String remarks;
	private int maxIdentifiction;
	private Timestamp createDate;


	private String categoryName;
	private String newAndOldName;

	//setterとgetter
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public int getGenreId() {
		return genreId;
	}
	public void setGenreId(int genreId) {
		this.genreId = genreId;
	}
	public int getRecommendedFlg() {
		return recommendedFlg;
	}
	public void setRecommendedFlg(int recommendedFlg) {
		this.recommendedFlg = recommendedFlg;
	}

	public String getNewAndOldId() {
		return newAndOldId;
	}
	public void setNewAndOldId(String newAndOldId) {
		this.newAndOldId = newAndOldId;
	}
	//	public BufferedImage getImage() {
//		return image;
//	}
//	public void setImage(BufferedImage image) {
//		this.image = image;
//	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}

	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public int getMaxIdentifiction() {
		return maxIdentifiction;
	}
	public void setMaxIdentifiction(int maxIdentifiction) {
		this.maxIdentifiction = maxIdentifiction;
	}
	public Timestamp getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getNewAndOldName() {
		return newAndOldName;
	}
	public void setNewAndOldName(String newAndOldName) {
		this.newAndOldName = newAndOldName;
	}


}
