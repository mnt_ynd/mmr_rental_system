package jp.mnt.dto;

public class RentalDetailDTO {
	//クラス変数
	private int rentalDetailNumber;
	private int rentalNumber;
	private int itemId;
	private int price;
	private int identificationNumber;

	//setterとgetter
	public int getRentalDetailNumber() {
		return rentalDetailNumber;
	}
	public void setRentalDetailNumber(int rentalDetailNumber) {
		this.rentalDetailNumber = rentalDetailNumber;
	}
	public int getRentalNumber() {
		return rentalNumber;
	}
	public void setRentalNumber(int rentalNumber) {
		this.rentalNumber = rentalNumber;
	}
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getIdentificationNumber() {
		return identificationNumber;
	}
	public void setIdentificationNumber(int identificationNumber) {
		this.identificationNumber = identificationNumber;
	}
}
