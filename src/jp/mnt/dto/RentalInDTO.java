package jp.mnt.dto;

public class RentalInDTO {
	
	String userIndex;
	String deliveryDate;
	String deliveryTimeId;
	String couponIndex;
	String discount;
	public String getUserIndex() {
		return userIndex;
	}
	public void setUserIndex(String userIndex) {
		this.userIndex = userIndex;
	}
	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getDeliveryTimeId() {
		return deliveryTimeId;
	}
	public void setDeliveryTimeId(String deliveryTimeId) {
		this.deliveryTimeId = deliveryTimeId;
	}
	public String getCouponIndex() {
		return couponIndex;
	}
	public void setCouponIndex(String couponIndex) {
		this.couponIndex = couponIndex;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
}
