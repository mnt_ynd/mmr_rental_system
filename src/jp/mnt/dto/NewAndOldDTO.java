package jp.mnt.dto;

public class NewAndOldDTO {
	//クラス変数
	private char newAndOldId;
	private String newAndOldName;

	//setterとgetter
	public char getNewAndOldId() {
		return newAndOldId;
	}
	public void setNewAndOldId(char newAndOldId) {
		this.newAndOldId = newAndOldId;
	}
	public String getNewAndOldName() {
		return newAndOldName;
	}
	public void setNewAndOldName(String newAndOldName) {
		this.newAndOldName = newAndOldName;
	}
}
